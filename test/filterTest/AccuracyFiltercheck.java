/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package filterTest;

import Service.constants.Service;
import Service.impl.ServiceFilterImpl;
import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.*;
import static org.junit.Assert.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Accuracy;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.LowerBound;
import sparqlfunctions.constants.Measurement;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.Range;
import sparqlfunctions.impl.SparqlImpl;
import sparqlfunctions.constants.Unit;
import sparqlfunctions.constants.UpperBound;

/**
 *
 * @author krishna
 */
public class AccuracyFiltercheck {

    @Test
    /**
	 * it is to show how the available services can be rejected on the basis of required values
         * of Accuracy and Range
         * we change the initial given constraint using function for reset and add property objects 
         * available in the Query class
     */
    public void testaccuracyfilterCheck() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testaccuracyfilterCheck");
        ServiceFilterImpl sv=new ServiceFilterImpl();
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("temperature_C",new Unit("degreeC"),new Concept("temperature"),""),new Measurement( (double)25));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.accuracy.value>20 & windchill_factor.unit.name=degreeC")),new Location("paris"));
        //to compute concept
        HashMap<Concept,FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //since wind_chill factor is not possible to convert to degreeC from kilocaloriePerSquaremetePerHour(since it is
        //not defined in the ontology)
        //degreeC value is returned after conversion from degreeF value(windchill_usa Fommula)
        assertEquals("Result",25,sparql.average(result),3);//degreeC conversion of 77(windchill_usa Formula value)
        //now we will create a virtual ArrayList of Services consisting only the Range and Accuracy values
        ArrayList<Service> serviceList=new ArrayList();
        ArrayList<Service> filterServiceList;
        serviceList.add(new Service(new Accuracy(new LowerBound(-15),new UpperBound(15)),new Range(new LowerBound(-15),new UpperBound(15))));
        serviceList.add(new Service(new Accuracy(new LowerBound(-18),new UpperBound(18)),new Range(new LowerBound(-18),new UpperBound(18))));
        serviceList.add(new Service(new Accuracy(new LowerBound(0),new UpperBound(21)),new Range(new LowerBound(0),new UpperBound(21))));
        serviceList.add(new Service(new Accuracy(new LowerBound(0),new UpperBound(29)),new Range(new LowerBound(0),new UpperBound(29))));
        System.out.println("filtered services by using initial given conditions");
        filterServiceList=sv.filterService(query,new Concept("windchill_factor") , serviceList);
        try {
                for(Service s:filterServiceList) {
                    s.getAccuracy().print();
                    s.getRange().print();
                }
        } catch(NullPointerException e) {

                System.out.println("No available Service can serve required conditions");

        }

        System.out.println("after adding a new required Accuracy in the Query");
        // we add a extra Accuracy condition
        query.addRequiredAccuracy(new Concept("windchill_factor"), new Accuracy(new UpperBound(-1)));
        filterServiceList=sv.filterService(query,new Concept("windchill_factor") , serviceList);
        try {
                for(Service s:filterServiceList) {
                    s.getAccuracy().print();
                    s.getRange().print();
                }
        } catch(NullPointerException e) {

                System.out.println("No available Service can serve required conditions");

        }

        System.out.println("after resetting all conditions");
        //we completely reset the constraints for the Query
        query.resetRequiredConstraint("windchill_factor.accuracy.value<-4 & windchill_factor.unit.name=degreeF");
        result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",77,sparql.average(result),3);//degreeF value
        filterServiceList.removeAll(filterServiceList);
        filterServiceList=sv.filterService(query,new Concept("windchill_factor") , serviceList);
        try {
                for(Service s:filterServiceList) {
                    s.getAccuracy().print();
                    s.getRange().print();
                }
        } catch(NullPointerException e) {

                System.out.println("No available Service can serve required conditions");

        }
    }
}
