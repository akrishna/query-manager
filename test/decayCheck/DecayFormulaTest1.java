/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decayCheck;

import sparqlfunctions.constants.Concept;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import org.junit.*;
import static org.junit.Assert.*;
import sparqlfunctions.constants.DecayFormula;


/**
 *
 * @author krishna
 */

public class DecayFormulaTest1 {


    @Test
    public void decayFormulaTest() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("decayFormulaTest");
        DecayFormula test= new DecayFormula();
         assertEquals("Result",.46,test.computeDecay(10,0.01,new Concept("noiselevel")),.1);
         //i have defined the formula for noice level as d = - ln(limit) / alpha
         //i pass alpha as 3 and limit as 0.01 
         //i.e. output= -log(0.01)/10= 2.303*2 /10=.46
    }

     @Test
    public void decayFormulaTest2() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("decayFormulaTest2");
        DecayFormula test= new DecayFormula();
         assertEquals("Result",0,test.computeDecay(10,0.001,new Concept("windchill_factor")),.1);
         //decay formula is not defined for this concept
         //so value of decayfactor comes out to be 0 for this concept
    }
}
