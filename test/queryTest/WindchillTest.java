/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import sparqlfunctions.impl.SparqlImpl;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Unit;
import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.junit.*;
import static org.junit.Assert.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Measurement;

/**
 *  
 * @author krishna
 */

/*
 * CALCULATION
 *
 * when we supply temperature as 25 degreeC & wind_speed as 50 meterPerSecond
 *
 * then result is
 *
 * by windchill_formula_USA windchill_factor=77 degreeF
 * & by windchill_formula_Original windchill_factor=249 kilocaloriePerSquaremetePerHour
 *
 * i use this calculation throughout the test
 *
 * 
 */
public class WindchillTest {
    /**
	 * we supply 3 elements in the HashMap each of concept pitchfactor,temperature,rain
         * pitchfactor=quality+1 ,temperature=pitchfactor+1 & wind_speed=rain+1
         * according to given values temperature= [25,24+1]=25 degreeC
         * & wind_speed= [49+1]=50 meterPerSecond
         * we compute the value as 249 in kilocaloriePerSquaremetePerHour and 77 in degreeF(or 25 in degreeC just
         * to check conversion also)
         * the result is returned according to required unit
	 */

    @Test
    public void testcomputeQueryConcept() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("temperature_C",new Unit("degreeC"),new Concept("temperature"),""),new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""),new Measurement( (double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument 
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeC")),new Location("paris"));
        //to compute concept
        HashMap<Concept,FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //since wind_chill factor is not possible to convert to degreeC from kilocaloriePerSquaremetePerHour(since it is
        //not defined in the ontology)
        //degreeC value is returned after conversion from degreeF value(windchill_usa Fomrmula)
        assertEquals("Result",25,sparql.average(result),3);//degreeC conversion of 77(windchill_usa Formula value)
                                                               
       
    }

     /**
	 * we supply only one element with concept as windchill_factor so that value is returned

	 */
        @Test
        public void testcomputeQueryConcept1() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept1");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("windchill_factor"),""), new Measurement((double)30));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeC")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",30,sparql.average(result),3);


    }
    /**
	 * we supply 2 elements in the HashMap each of concept temperature,wind_speed
         * according to given values temperature= [25]=25 degreeC
         * & wind_speed= [50]=50 meterPerSecond
         * we compute the value as 249 in kilocaloriePerSquaremetePerHour and 77 in degreeF(or 25 in degreeC just
         * to check conversion also)
         * the result is returned according to required unit
	 */

            @Test
        public void testcomputeQueryConcept2() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept2");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeF"),new Concept("temperature"),""), new Measurement((double)77));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeC")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //since wind_chill factor is not possible to convert to degreeC from kilocaloriePerSquaremetePerHour(since it is
        //not defined in the ontology)
        //degreeC value is returned after conversion from degreeF value(windchill_usa Formula)
        assertEquals("Result",25,sparql.average(result),3);


    }

               /**
	 * we supply 3 elements in the HashMap each of concept temperature,wind_speed,windchill_factor
         * according to given values temperature= [25]=25 degreeC
         * & wind_speed= [50]=50 meterPerSecond
         * we compute the value as 249 in kilocaloriePerSquaremetePerHour and 77 in degreeF(or 25 in degreeC just
         * to check conversion also)
         * windchill_factor=[25,30]=27.5 degreeC
         * the result is returned according to required unit
	 */


                @Test
        public void testcomputeQueryConcept3() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept3");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("windchill_factor"),""), new Measurement((double)30));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeC")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //since wind_chill factor is not possible to convert to degreeC from kilocaloriePerSquaremetePerHour(since it is
        //not defined in the ontology)
        //degreeC value is returned after conversion m degreefro
        assertEquals("Result",27.5,sparql.average(result),1);//(25+30)/2
                                                             //30 is passed in hashmap & 25 is from windchill_usa
                                                             //Formula (degreeC equivalent of 77 degreeF)


    }

                /**
	 * we supply 2 elements in the HashMap each of concept pitchfactor,rain
         * according to given values temperature= [24+1]=25 degreeC
         * & wind_speed= [49+1]=50 meterPerSecond
         * we compute the value as 249 in kilocaloriePerSquaremetePerHour and 77 in degreeF(or 25 in degreeC just
         * to check conversion also)
         * windchill_factor=[25,30]=27.5 degreeC
         * the result is returned according to required unit
	 */

        @Test
        public void testcomputeQueryConcept4() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept4");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeF")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //since wind_chill factor is not possible to convert to degreeF from kilocaloriePerSquaremetePerHour(since it is
        //not defined in the ontology)
        //degreeF value is returned (from the windchill_formula_USA )

        assertEquals("Result",77,sparql.average(result),3);


    }

    /*
     * windchill_factor is present in too many units in the parameter value hashmap
    */
        @Test
    public void testcomputeQueryConcept5() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept5");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("windchill_factor"),""), new Measurement((double)30));
        m1.put(new Parameter("anything",new Unit("X"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("kilocaloriePerSquaremetePerHour"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeF")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //degreeF value is returned
        assertEquals("Result",81,sparql.average(result),3);//(86+77)/2 -->86 is the degreeF equivalent of 30
                                                           //and 77 is the result of windchill_formula_USA



    }
        /*
         * same as testcomputeQueryConcept3() ,only difference is here we are getting one of the
         * parameters via expansion and othe parameter by directly search in HashMap
         */
            @Test
        public void testcomputeQueryConcept6() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept6");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("windchill_factor"),""), new Measurement((double)30));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeC")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //degreeC value is returned after conversion from 77(windchill_formula_usa Formula)
        assertEquals("Result",27.5,sparql.average(result),1);


    }
    /*
     * here we dont have sufficient parameters available in HashMap for computing windchill_factor
     * so it cant be computed
     */
                @Test
        public void testcomputeQueryConcept7() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept7");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=degreeC")),new Location("paris"));
        //to compute concept
        HashMap<Concept,FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //since wind_chill factor is not possible to convert to degreeC from kilocaloriePerSquaremetePerHour(since it is
        //not defined in the ontology)
        assertEquals("Result",null,sparql.average(result));


    }
            /*
             * same as testcomputeQueryConcept6(),the difference is that it is without a settor object and hence return
             * first available unit(if they are not intra convertible)
             * can be affected by sparql
             */


                @Test
        public void testcomputeQueryConcept8() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept8");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter 
        //value hashMap when no required unit is supplied we start by trying converting to first available unit
        //since no unit of windchill is inter convertible then any result may return depending upon the ordering
        //of sparql results
        Iterator<Concept> it2 = result.keySet().iterator();
        String check=result.get(it2.next()).getResultUnit().getUnitName();
        if(check.equals("degreeF")) {
                assertEquals("Result",77,sparql.average(result),3);
        }
        else if(check.equals("kilocaloriePerSquaremetePerHour")) {
                assertEquals("Result",249,sparql.average(result),3);
        }
        else
            fail();


    }

    /*
     * same as testcomputeQueryConcept5 except required unit
     */
        @Test
    public void testcomputeQueryConcept9() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept9");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("windchill_factor"),""), new Measurement((double)30));
        m1.put(new Parameter("anything",new Unit("X"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("kilocaloriePerSquaremetePerHour"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=kilocaloriePerSquaremetePerHour")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //degreeF value is returned
        assertEquals("Result",149,sparql.average(result),3);//(50+249)/2 -->50 is value pass in parametr value HashMap
                                                           //and 249 is the result of windchill_formula_Original



    }

  /*
     * same as testcomputeQueryConcept5 except required unit
     */
        @Test
    public void testcomputeQueryConcept10() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept10");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("windchill_factor"),""), new Measurement((double)30));
        m1.put(new Parameter("anything",new Unit("X"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("kilocaloriePerSquaremetePerHour"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=X")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //result in x unit is returned
        assertEquals("Result",50,sparql.average(result),3);



    }


  /*
     * same as testcomputeQueryConcept5 except required unit
     */
        @Test
    public void testcomputeQueryConcept11() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept11");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("windchill_factor"),""), new Measurement((double)30));
        m1.put(new Parameter("anything",new Unit("X"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("kilocaloriePerSquaremetePerHour"),new Concept("windchill_factor"),""), new Measurement((double)50));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("rain"),""), new Measurement((double)49));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        ArrayList<Concept> conceptsToMeasure = new ArrayList<Concept>();
	conceptsToMeasure.add(new Concept("windchill_factor"));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.name=Y")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
       //first available unit will be returned depending on Sparql
        Iterator<Concept> it2 = result.keySet().iterator();
        String check=result.get(it2.next()).getResultUnit().getUnitName();
        if(check.equals("degreeF")) {
                assertEquals("Result",81.5,sparql.average(result),3);
        }
        else if(check.equals("degreeC")) {
                assertEquals("Result",27.5,sparql.average(result),3);
        }
        else if(check.equals("X")) {
                assertEquals("Result",50,sparql.average(result),3);
        }
        else if(check.equals("kilocaloriePerSquaremetePerHour")) {
                assertEquals("Result",149,sparql.average(result),3);
        }
        else
            fail();




    }


       
}
