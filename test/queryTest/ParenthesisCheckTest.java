/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import sparqlfunctions.impl.SparqlImpl;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Unit;
import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.HashMap;
import org.junit.*;
import static org.junit.Assert.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Measurement;


/**
 *  
 * @author krishna
 */
//this test is for checking the order of paranthesis in setter conditions
//set of unit constraint will be printed in console
//
public class ParenthesisCheckTest {
      

    @Test
     /**
         * in this test two different units are provided with a & between which result in a bad Query object
         * (a.unit=x & a.unit=y ) this is the pattern of boolean expression
         * null value is returned with a message "THE QUERY FAILS"
	 */
    public void testcomputeQueryConcept() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept()");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""),new Measurement( (double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument 
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.unit.name=degreeC & windchill_factor.unit.name=x)")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);
        }

                                                               
       
    

     @Test

      /**
         * in this test two same units are provided with a & between which result in a correct Query object
         * (a.unit=x & a.unit=x ) this is the pattern of boolean expression
         * desired(requested) unit is thus x
         * that value is returned in the requested unit
	 */
    public void testcomputeQueryConcept3() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept3");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.unit.equals=degreeC & windchill_factor.unit.equals=degreeC ) & (windchill_factor.accuracy.value>3 & windchill_factor.accuracy.value<899 )")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",25,sparql.average(result),3);


    }

       @Test

       /**
         * this is an example of bad Query object
         * (a.unit=x & a.unit=y )& a.unit=degreeC this is the pattern of boolean expression
         * null value is returned with a message "THE QUERY FAILS"
	 */
    public void testcomputeQueryConcept4() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept4");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("( windchill_factor.unit.equals=X & windchill_factor.unit.equals=Y) & windchill_factor.unit.equals=degreeC")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);

    }

         @Test


      /**
         * (a.unit=x & a.unit=Y )| a.unit=degreeC this is the pattern of boolean expression and hence
         *  this is an example of correct Query object
         * desired(requested) unit is thus degreeC
         * that value is returned in the requested unit
	 */
    public void testcomputeQueryConcept2() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept2");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.equals=X & windchill_factor.unit.equals=Y) | windchill_factor.unit.equals=degreeC")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",25,sparql.average(result),3);


    }
    @Test
                /**
         * this is an example of bad Query object
         * (a.unit=x & a.unit=y )& (a.unit=degreeC & a.unit=degreeF) this is the pattern of boolean expression
         * null value is returned with a message "THE QUERY FAILS"
	 */
    public void testcomputeQueryConcept5() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept5");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.equals=X & windchill_factor.unit.equals=Y) & (windchill_factor.unit.equals=degreeC | windchill_factor.unit.equals=degreeF)")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);

    }
    @Test
                    /**
         * this is an example of correct Query object
         * (a.unit=x & a.unit=y )| (a.unit=z | a.unit=degreeC) this is the pattern of boolean expression
         * desired unit is thus degreeC
	 */
    public void tescomputeQueryConcept6() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept6");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("windchill_factor.unit.equals=X & windchill_factor.unit.equals=Y) | (windchill_factor.unit.equals=z | windchill_factor.unit.equals=degreeC)")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",25,sparql.average(result),3);
 
    }

        @Test
         /**
         * this is an example of correct Query object
         * (a.unit=degreeC & (a.unit=unknown | a.unit=degreeF)) | a.unit=degreeC this is the pattern of boolean expression
         * desired unit is thus degreeC
	 */
    public void tescomputeQueryConcept7() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept7");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.unit.name=degreeC & (windchill_factor.unit.name=unknown | windchill_factor.unit.name=degreeF)) | windchill_factor.unit.name=degreeC")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",25,sparql.average(result),3);

    }

         @Test
                    /**
         * this is an example of correct Query object
         * (a.accuracy=3 & (a.range=4 | a.range=7)) | a.unit=degreeC this is the pattern of boolean expression
         * desired unit is thus degreeC
	 */
    public void tescomputeQueryConcep8() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept8");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.accuracy.value=3 & (windchill_factor.accuracy.value=4 & windchill_factor.range.value=7)) | windchill_factor.unit.name=degreeF")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        //degreeF value is returned
        assertEquals("Result",77,sparql.average(result),3);

    }


          @Test
                    /**
         * this is an example of correct Query object
         * (a.accuracy=3 & (a.range=4 & a.range=7)) | a.unit=degreeC this is the pattern of boolean expression
         * desired unit is thus degreeC
	 */
    public void tescomputeQueryConcep9() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept9");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.accuracy.value=3 & (windchill_factor.range.value=4 | windchill_factor.range.value=7)) | windchill_factor.unit.name=degreeC")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        // degreeC value is returned
                assertEquals("Result",25,sparql.average(result),3);

    }

         @Test
     /**
         * in this test two different range are provided with a & between which result in a bad Query object
         * (a.accuracy=2 & a.accuracy=12 ) this is the pattern of boolean expression
         * null value is returned with a message "THE QUERY FAILS"
	 */
    public void testcomputeQueryConcept10() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept10()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.accuracy.value=2 & windchill_factor.accuracy.value=12)")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);
        }



         @Test
     /**
         * in this test two different range are provided with a & between which result in a bad Query object
         * (a.range=2 & a.range=12 ) this is the pattern of boolean expression
         * null value is returned with a message "THE QUERY FAILS"
	 */
    public void testcomputeQueryConcept11() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept11()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.range.value=2 & windchill_factor.range.value=12)")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);

    }

    @Test
     /**
      * this is a valid Query
	 */
    public void testcomputeQueryConcept12() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept12()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql=new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.range.value=2 & windchill_factor.accuracy.value=12) & windchill_factor.unit.name=degreeC ")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",25,sparql.average(result),3);

    }

        @Test
     /**
      * this is not a valid Query
	 */
    public void testcomputeQueryConcept13() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept13()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.range.value=2 & windchill_factor.range.value=12) & windchill_factor.unit.name=degreeF ")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);
    }

         @Test
     /**
      * this is not a valid Query due to suntax precheck
	 */
    public void testcomputeQueryConcept14() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept14()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql=new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor,range.value=2 & windchill_factor.accuracy.value=12) & windchill_factor.unit.name=degreeC ")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);

    }

                  @Test
     /**
      * this is not a valid Query due to syntax precheck of Setter
	 */
    public void testcomputeQueryConcept15() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept15()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql=new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.range.value=2 & windchill_factoraccuracy.value=12) & windchill_factor.unit.name=degreeC ")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);

    }

                           @Test
     /**
      * this is not a valid Query due to syntax precheck
	 */
    public void testcomputeQueryConcept16() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept16()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql=new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("(windchill_factor.range.value=2 & windchill_factor.accuracy.value-12) & windchill_factor.unit.name=degreeC ")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",null,result);

    }

     @Test
     /**
      * this is a valid Query
      * it is to show the intersection of the interval of requested range and accuracy
      */
    public void testcomputeQueryConcept17() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("testcomputeQueryConcept17()");
       HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql=new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)25));
        m1.put(new Parameter("anything",new Unit("meterPerSecond"),new Concept("wind_speed"),""), new Measurement((double)50));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("((windchill_factor.accuracy.value>2 & windchill_factor.accuracy.value<50) | (windchill_factor.accuracy.value>100 & windchill_factor.accuracy.value<200))| (windchill_factor.accuracy.value>2 & windchill_factor.unit.name=degreeC ")),new Location("paris"));
        //m1 is the Parameter value hashMap
        //average is the fusion functions type
        query.getSetter().getConstraint().printAccuracyHashMap();
        query.getSetter().getConstraint().printRangeHashMap();
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");
        assertEquals("Result",25,sparql.average(result),3);

    }

}
