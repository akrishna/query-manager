/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.ArrayList;
import org.junit.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Concept;


/**
 *
 * @author krishna
 */
//each test will print all subconcept for a concept
public class SubConceptCheck3 {
      /**
	 * windchill_factor(uppermost concept i.e. head of the concept tree) is supplied in Concept
	 * print all subconcept till the leaf for windchill_factor
         * list will contain temperature,wind_speed,pitchfactor,quality,rain
	 */
     @Test
    public void testcomputeQueryConcept() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept");
        QueryCompositionManagerImpl query1=new QueryCompositionManagerImpl(new Selector(new Concept("windchill_factor")),new Setter(new Where("temperature.unit.equals=degreeC")),new Location("paris"));
        ArrayList<Concept> value1=query1.expandConcept();//check all expended concept
        for(int i=0;i<value1.size();i++) {
            System.out.println(value1.get(i).getConceptName());
        }


    }




}
