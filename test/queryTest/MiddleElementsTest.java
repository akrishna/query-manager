/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import sparqlfunctions.impl.SparqlImpl;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Unit;
import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.HashMap;
import org.junit.*;
import static org.junit.Assert.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Measurement;


/**
 *
 * @author krishna
 */
//here we will check for concepts which are not the head node or the leaf in the concept tree
public class MiddleElementsTest {
     /**
	 * we supply 3 elements in the HashMap each of concept pitchfactor,quality,temperature all in degreeC
         * pitchfactor=quality+1 and temperature=pitchfactor+1
         * according to given values pitchfactor= [29+1,24]=27
         * & temperature= [27+1,30]=29 degreeC and 82 is its degreeF unit value
         * the result is returned according to required unit
	 */


    @Test
    public void testcomputeQueryConcept() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("quality"),""), new Measurement((double)29));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("pitchfactor"),""), new Measurement((double)24));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)30));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("temperature")),new Setter(new Where("temperature.unit.equals=degreeF")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",82,sparql.average(result),3);

        query=new QueryCompositionManagerImpl(new Selector(new Concept("temperature")),new Setter(new Where("temperature.unit.equals=degreeC")),new Location("paris"));
        //to compute concept
        result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",29,sparql.average(result),3);

        query=new QueryCompositionManagerImpl(new Selector(new Concept("temperature")),new Setter(new Where("temperature.unit.equals=unknown")),new Location("paris"));
        //to compute concept
        result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",29,sparql.average(result),3);


    }

/**
	 * we supply 3 elements in the HashMap each of concept pitchfactor,quality,temperature all in degreeC
         * pitchfactor=quality+1 and temperature=pitchfactor+1
         * according to given values pitchfactor= [28+1]=29
         * & temperature= [29+1,40]=35 degreeC and 95 is its degreeF unit value
         * the result is returned according to required unit
	 */


    @Test
    public void testcomputeQueryConcept2() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept2");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("quality"),""), new Measurement((double)28));
        m1.put(new Parameter("anything",new Unit("degreeC"),new Concept("temperature"),""), new Measurement((double)40));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("temperature")),new Setter(new Where("temperature.unit.equals=degreeF")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",95,sparql.average(result),3);

        query=new QueryCompositionManagerImpl(new Selector(new Concept("temperature")),new Setter(new Where("temperature.unit.equals=degreeC")),new Location("paris"));
        //to compute concept
        result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",35,sparql.average(result),3);


    }


}
