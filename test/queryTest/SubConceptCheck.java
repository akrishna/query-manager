/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.ArrayList;
import org.junit.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Concept;


/**
 *
 * @author krishna
 */
//each test will print all subconcept for a concept
public class SubConceptCheck {
 
              /**
	 * quality(leaf node in the concept tree) is supplied in Concept
	 * print only the concept passed since no expansion is possible
         *  list will contain only quality
	 */
     @Test
    public void testcomputeQueryConcept3() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept3");
        QueryCompositionManagerImpl query3=new QueryCompositionManagerImpl(new Selector(new Concept("quality")),new Setter(new Where("temperature.unit.equals=degreeC")),new Location("paris"));
        ArrayList<Concept> value3=query3.expandConcept();//check all expended concept
        for(int i=0;i<value3.size();i++) {
            System.out.println(value3.get(i).getConceptName());
        }



    }

    

    
}
