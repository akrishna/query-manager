/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import sparqlfunctions.impl.SparqlImpl;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Unit;
import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.HashMap;
import org.junit.*;
import static org.junit.Assert.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Measurement;


/**
 *  
 * @author krishna
 */
//in this class we will test for the concept that does not have a expansion i.e. leaf in Query structure
public class LeafConceptCheck {
      /**
	 * we supply a single parameter in the hashmap which have the concept quality with unit degreeF
         * required unit is degreeF in one case and of (a|b) type in another
         * the result is returned according to required unit
	 */


    @Test
    public void testcomputeQueryConcept() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("x",new Unit("degreeF"),new Concept("quality"),""), new Measurement((double)250));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("quality")),new Setter(new Where("quality.unit.equals=degreeF")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //here Quality is the Concept which dont have any further expansion
        assertEquals("Result",250,sparql.average(result),1);

        query=new QueryCompositionManagerImpl(new Selector(new Concept("quality")),new Setter(new Where("quality.unit.equals=unknown | quality.unit.equals=degreeC")),new Location("paris"));
        //to compute concept
        result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",121,sparql.average(result),1);


    }
    
        /**
	 * we supply a single parameter in the hashmap which have 2 concept(having different units) quality with unit degreeF
         * required unit is degreeF in one case and of (a|b) type in another
         * the result is returned according to required unit
	 */


    @Test
    public void testcomputeQueryConcept2() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept2");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("x",new Unit("degreeF"),new Concept("quality"),""), new Measurement((double)250));
        //94 is degreeC equivalent 200 degreeF
        m1.put(new Parameter("y",new Unit("degreeC"),new Concept("quality"),""), new Measurement((double)94));
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("quality")),new Setter(new Where("quality.unit.equals=degreeF")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        //here Quality is the Concept which dont have any further expansion
        assertEquals("Result",225,sparql.average(result),1);//(200+250)/2

        query=new QueryCompositionManagerImpl(new Selector(new Concept("quality")),new Setter(new Where("quality.unit.equals=unknown | quality.unit.equals=degreeC")),new Location("paris"));
        //to compute concept
        result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",107,sparql.average(result),1);//degreeC euqivalent of (200+250)/2

        

    }
    
}
