/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import sparqlfunctions.impl.SparqlImpl;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Unit;
import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.HashMap;
import org.junit.*;
import static org.junit.Assert.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Measurement;

/**
 *  
 * @author krishna
 */

public class NoiseLevelCheck {
      /**
	 * we supply a single parameter in the hashmap which have the concept noiselevel
         * required unit is db
	 */

    @Test
    public void testcomputeQueryConcept() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("x",new Unit("db"),new Concept("noiselevel"),""), new Measurement((double)250));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument 
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("noiselevel")),new Setter(new Where("noiselevel.unit.equals=db")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",250,sparql.average(result),3);
                                                               
       
    } 
    

    /**
	 * we supply a single parameter in the hashmap which have the concept noiselevel
         * required unit is a insignificant unit(not available)
         * first value will be returned
	 */

    @Test
    public void testcomputeQueryConcept2() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept2");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("x",new Unit("db"),new Concept("noiselevel"),""), new Measurement((double)250));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("noiselevel")),new Setter(new Where("noiselevel.unit.equals=anything")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",250,sparql.average(result),3);


    }

    /**
	 * we supply a single parameter in the hashmap which have the concept noiselevel
         * WITHOUT A SETTER
         * FIRST VALUE IS RETURNED
	 */

    @Test
    public void testcomputeQueryConcept3() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept3");
        HashMap<Parameter,Measurement> m1=new HashMap<Parameter,Measurement>();
        SparqlImpl sparql= new SparqlImpl();
        m1.put(new Parameter("x",new Unit("db"),new Concept("noiselevel"),""), new Measurement((double)250));
        //above is the formation of Parameter value HashMap
        //to be passed in the function query.computeQueryConcept as argument
        QueryCompositionManagerImpl query=new QueryCompositionManagerImpl(new Selector(new Concept("noiselevel")),new Location("paris"));
        //to compute concept
        HashMap<Concept, FinalResult> result=query.computeQueryConcept(m1,"average");//m1 is the Parameter value hashMap
        assertEquals("Result",250,sparql.average(result),3);


    }


}
