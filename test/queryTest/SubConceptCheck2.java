/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package queryTest;

import query.constants.Setter;
import query.constants.Where;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.ArrayList;
import org.junit.*;
import query.constants.Selector;
import query.impl.QueryCompositionManagerImpl;
import sparqlfunctions.constants.Concept;


/**
 *
 * @author krishna
 */
//each test will print all subconcept for a concept
public class SubConceptCheck2 {


           /**
	 * temperature(middle node in the concept tree) is supplied in Concept
	 * print all subconcept till the leaf for temperature
         * list will contain temperature,pitchfactor,quality
	 */
     @Test
    public void testcomputeQueryConcept2() throws UnparsableExpressionException, UnknownFunctionException {
        System.out.println("computeQueryConcept2");
        QueryCompositionManagerImpl query2=new QueryCompositionManagerImpl(new Selector(new Concept("temperature")),new Setter(new Where("temperature.unit.equals=degreeC")),new Location("paris"));
        ArrayList<Concept> value2=query2.expandConcept();//check all expended concept
        for(int i=0;i<value2.size();i++) {
            System.out.println(value2.get(i).getConceptName());
        }


    }






}
