/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Service.constants;

import sparqlfunctions.constants.Accuracy;
import sparqlfunctions.constants.Range;

/**
 *
 * @author krishna
 */
public class Service {
        private Accuracy accuracy;
        private Range range;

        public Service(Accuracy serviceAccuracy,Range serviceRange) {
            accuracy=serviceAccuracy;
            range=serviceRange;
        }

        public Accuracy getAccuracy() {
            return accuracy;
        }

        public Range getRange() {
            return range;
        }
}
