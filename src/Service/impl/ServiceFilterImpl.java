/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Service.impl;

import Service.constants.Service;
import Service.interfaces.ServiceFilter;

import java.util.ArrayList;
import query.impl.QueryCompositionManagerImpl;


import sparqlfunctions.constants.Accuracy;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.IntervalOperations;
import sparqlfunctions.constants.Range;

/**
 *
 * @author krishna
 */
public class ServiceFilterImpl implements ServiceFilter {
       /**
	 * the method is called to filter the available services based on the required values of Accuracy and Range
	 * @param mainQuery: the Query object
         * @param concept: the Concept which is to be computed
         * @serviceList: ArrayList of all available Services
	 * @return: ArrayList of available services which can serve the required conditions for Accuracy and Range
	 */
    @Override
    public ArrayList<Service> filterService(QueryCompositionManagerImpl mainQuery,Concept concept,ArrayList<Service> serviceList) {
           boolean foundAccuracy=false;//will tell if the Accuracy conditions can be satisfied
           boolean foundRange=false;//will tell if the Range conditions can be satisfied
           ArrayList<Accuracy> allowedAccuracyList;
           ArrayList<Range> allowedRangeList;
           allowedAccuracyList=mainQuery.getSetter().getConstraint().getAccuracyHashMap().get(concept.getConceptName());
           allowedRangeList=mainQuery.getSetter().getConstraint().getRangeHashMap().get(concept.getConceptName());
           ArrayList<Service> filteredServiceList = new ArrayList();
           for(int i=0;i<serviceList.size();i++) {
               foundRange=false;
               foundAccuracy=false;
            try {
               for(Accuracy accuracy : allowedAccuracyList) {
                   //if the intersection is not empty then declare that this available service can fullfill
                   //the required conditions for accuracy
                   if(IntervalOperations.intersection(serviceList.get(i).getAccuracy(),accuracy)!=null) {
                        foundAccuracy=true;
                        break;
                   }
               }
           } catch(NullPointerException e) {

               foundAccuracy=true;
           }

           try {
               for(Range range : allowedRangeList) {
                    //if the intersection is not empty then declare that this available service can fullfill
                   //the required conditions for range
                   if(IntervalOperations.intersection(serviceList.get(i).getRange(),range)!=null) {
                        foundRange=true;
                        break;
                   }
               }
           } catch(NullPointerException e) {
               foundRange=true;
           }
                //if both the conditions can be satisfied
               if(foundAccuracy==true && foundRange==true) {
                   
                   filteredServiceList.add(serviceList.get(i));
               }


        }
           return filteredServiceList;

    }
}
