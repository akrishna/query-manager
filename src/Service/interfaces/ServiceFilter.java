/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Service.interfaces;

import Service.constants.Service;

import java.util.ArrayList;
import query.impl.QueryCompositionManagerImpl;



import sparqlfunctions.constants.Concept;

/**
 *
 * @author krishna
 */
public interface ServiceFilter {

     /**
	 * the method is called to filter the available services based on the required values of Accuracy and Range
	 * @param mainQuery: the Query object
         * @param concept: the Concept which is to be computed
         * @serviceList: ArrayList of all available Services
	 * @return: ArrayList of available services which can serve the required conditions for Accuracy and Range
     */
     ArrayList<Service> filterService(QueryCompositionManagerImpl mainQuery,Concept concept,ArrayList<Service> serviceList);

}
