/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sparqlfunctions.impl;


import com.hp.hpl.jena.query.*;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import fr.inria.arles.choreos.iots.common.Location;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import query.impl.SubQueryGeneratorImpl;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.DataFusionGeneratorImpl;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Formula;
import sparqlfunctions.constants.Measurement;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.Unit;
import sparqlfunctions.interfaces.Sparql;

/**
 * @author krishna
 */
public class SparqlImpl implements Sparql {
                DataFusionGeneratorImpl df;
                private HashMap<Parameter,Measurement>parameterValueHashMap;
                private Model model;
                
                //constructor for the object class
                public SparqlImpl()  {
                
                String inputFileName="content/sensorsAndActuators.rdf";
                String inputFileName1="content/unitConversion.rdf";
        	try {
                        model = ModelFactory.createDefaultModel();
                        File f = new File(inputFileName);File f1 = new File(inputFileName1);
                        InputStream i;InputStream j;
                        i = new FileInputStream(f);j = new FileInputStream(f1);
                        model.read(i, null);    model.read(j, null);
                        i.close();

        	} catch (IOException e) {
                        System.out.println( e.getMessage());
                }
                 
     
    
                
                }

            /**
	 * the method is called to get mathematical expression of decay Formula of the concept.
	 * @param concept: Concept object
	 * @return the decay Formula expression
	 */
            public String getDecayFormulaExpression(Concept concept) {
                            int j;
                            String solution;
                            //Create a new query to find out all the graph models relatives of windchill_factor individual
                            String queryString="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                               "SELECT ?formula " +
                                               "WHERE {" +
                                               " base:"+concept.getConceptName()+"  base:DecayFormula_expression ?formula "+
                                               " }";
                                try {
                                         ResultSet results=sparqlQueryExecution(queryString);
                                         QuerySolution soln = results.next() ;
                                         //convert to proper formula string
                                         solution=soln.toString();
                                         j=solution.indexOf('=');
                                         solution=solution.substring(j+1);
                                         j=solution.indexOf('=');
                                         solution=solution.substring(j+1);
                                         j=solution.indexOf('"');
                                         solution=solution.substring(0,j);
                                         //solution=this.parse(solution);
                                } catch (NullPointerException e) {
                                         System.err.println("propety(base:DecayFormula_expression) does not exit in ontology for the supplied Concept");
                                         return null;
                                }
                                 catch (NoSuchElementException e) {
                                         System.err.println("propety(base:DecayFormula_expression) does not exit in ontology for the supplied Concept");
                                         return null;
                                }


                                    return solution;
                }
          
                /**
	 * fetch out all the functions Formula object that can be used in evaluating physical_concept 
	 * @param physical_concept: a Concept object for which functions need to be determined  
	 * @return then ArrayList of Formula objects
	 */
             
    @Override
            public  ArrayList<Formula> getFormulaList(Concept physical_concept) {
                              ArrayList<Formula> formula=new ArrayList();
                              String formulaName;
                              String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                    "SELECT ?functions " +
                                                    "WHERE {" +
                                                    "     base:"+physical_concept.getConceptName()+" base:provided_by ?functions . " +
                                                    "      }";

                              ResultSet results=sparqlQueryExecution(queryString);
                              if(results.hasNext()==false) {
                                  return null;
                              }
                              for ( ; results.hasNext(); ) 
                                  try {              
                                           QuerySolution soln = results.next() ;
                                           //conver to proper formula string
                                           formulaName=soln.toString();
                                           int i=formulaName.indexOf('#');
                                           int k=formulaName.indexOf('>');
                                           formulaName=formulaName.substring(i+1,k);
                                           formula.add(new Formula(
                                                   getInputParameters(
                                                   formulaName),
                                                   formulaName,
                                                   getMathematicalExpression(
                                                   formulaName),
                                                   getComputeConcept(
                                                   formulaName),
                                                   getOutputParameter(
                                                   formulaName),
                                                   getUnits(
                                                   formulaName)));
                                  }  catch (NullPointerException e) {
                                         System.err.println("no formula is their which compute the given concept");
                                         return null;

                                  } catch (NoSuchElementException e) {
                                         System.err.println("no formula is their which compute the given concept");
                                         return null;
                                  }
                               return formula;

            }
            /**
	 * the method is called to get string formula[mathematical expression]of a function from its name
	 * @param formulaName: formula name 
	 * @return the mathematical expression
	 */
            public String getMathematicalExpression(String formulaName) {
                            int j;
                            String solution;
                            //Create a new query to find out all the graph models relatives of windchill_factor individual
                            String queryString="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                               "SELECT ?formula " +
                                               "WHERE {" +
                                               " base:"+formulaName+"  base:mathematical_expression ?formula "+
                                               " }";
                                try {            
                                         ResultSet results=sparqlQueryExecution(queryString);
                                         QuerySolution soln = results.next() ;
                                         //convert to proper formula string
                                         solution=soln.toString();
                                         j=solution.indexOf('=');
                                         solution=solution.substring(j+1);
                                         j=solution.indexOf('=');
                                         solution=solution.substring(j+1);
                                         j=solution.indexOf('"');
                                         solution=solution.substring(0,j);
                                         //solution=this.parse(solution);
                                } catch (NullPointerException e) {
                                         System.err.println("propety(mathematical_expression) does not exit in ontology for the supplied formula");
                                         return null;

                                } catch (NoSuchElementException e) {
                                         System.err.println("propety(mathematical_expression) does not exit in ontology for the supplied formula");
                                         return null;
                                }
                                   
                                   
                                    return solution;
                }

               /**
	 * fetch out the output parameter of a formula
	 * @param formula: a formula name  
	 * @return the output Parameter object 
	 */
  
            public Parameter getOutputParameter(String formulaName) {           
                            String parameterName;
                            Parameter parameter;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                "SELECT ?outputUnit " +
                                                "WHERE {" +
                                                "    base:"+formulaName+" base:has_output ?outputUnit  . " +
                                                "      }";
                                try {    
                                         ResultSet results=sparqlQueryExecution(queryString);
                                         QuerySolution soln = results.next() ;
                                         //convert to proper format
                                         parameterName=soln.toString();
                                         int i=parameterName.indexOf('#');
                                         parameterName=parameterName.substring(i+1);
                                         i=parameterName.lastIndexOf('>');
                                         parameterName=parameterName.substring(0,i);
                                         parameter=new Parameter(
                                                 parameterName,
                                                 getUnits(
                                                 parameterName),
                                                 getPhysicalConcept(
                                                 parameterName),
                                                 getSymbol(
                                                 parameterName));
                                } catch (NullPointerException e) {
                                         System.err.println("given formula doesn't have any output parameter");
                                         return null;

                                } catch (NoSuchElementException e) {
                                         System.err.println("given formula doesn't have any output parameter");
                                         return null;
                                }
                             return parameter;
               }
               
        
            /**
	 * fetch out the list of input parameters of a formula
	 * @param formula: a Formula name
	 * @return the ArrayList of Parameter objects 
	 */
  
            public  ArrayList<Parameter> getInputParameters(String formulaName) {           
                            String parameterName;
                            ArrayList<Parameter> parameter = new ArrayList() ;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                "SELECT ?input " +
                                                "WHERE {" +
                                                "    base:"+formulaName+" base:has_input ?input  . " +
                                                "      }";

                            ResultSet results=sparqlQueryExecution(queryString);
                            for ( ; results.hasNext();) 
                                  try {         
                                            QuerySolution soln = results.next() ;
                                            //convert to proper format
                                            parameterName=soln.toString();
                                            int i=parameterName.indexOf('#');
                                            parameterName=parameterName.substring(i+1);
                                            i=parameterName.lastIndexOf('>');
                                            parameterName=parameterName.substring(0,i);
                                            parameter.add(new Parameter(
                                                    parameterName,
                                                    getUnits(
                                                    parameterName),
                                                    getPhysicalConcept(
                                                    parameterName),
                                                    getSymbol(
                                                    parameterName)));
                                  
                                  } catch (NullPointerException e) {
                                         System.err.println("given formula doesn't have any input parameter");
                                         return null;

                                  } catch (NoSuchElementException e) {
                                         System.err.println("given formula doesn't have any input parameter");
                                         return null;
                                  }
                             return parameter;
                 }
                
            /**
            * fetch out the symbol of the formula input parameter
            * @param input: parameter name
            * @return the symbol of the parameter
            */

               public  String getSymbol(String input) {               
                            
                            String symbol  ;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                    "SELECT ?symbol " +
                                                    "WHERE {" +
                                                    "    base:"+input+" base:symbol ?symbol . " +
                                                    "      }";
                                try {                
                                         ResultSet results=sparqlQueryExecution(queryString);
                                         QuerySolution soln = results.next() ;
                                         //conver to proper format
                                         symbol=soln.toString();
                                         int k=symbol.indexOf('"');
                                         int j=symbol.lastIndexOf('"');
                                         symbol=symbol.substring(k+1,j);
                                         return(symbol);
                                } catch (NullPointerException e) {
                                         System.err.println("parameter doesn't have a symbol");
                                         return null;

                                } catch (NoSuchElementException e) {
                                         System.err.println("parameter doesn't have a symbol");
                                         return null;
                                }
                   
                   }
               /**
	 * fetch out the concept linked with the function input parameter
	 * @param input: parameter name
	 * @return the Concept object which is the physical_concept of the parameter
	 */
 
            public Concept getPhysicalConcept(String input) {               

                            String conceptName;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                    "SELECT ?concept " +
                                                    "WHERE {" +
                                                    "    base:"+input+" base:physical_concept ?concept . " +
                                                    "      }";
                                try {    
                                         ResultSet results=sparqlQueryExecution(queryString);
                                         QuerySolution soln = results.next() ;
                                         //conver to proper formula string
                                         conceptName=soln.toString();
                                         int k=conceptName.indexOf('#');
                                         int j=conceptName.lastIndexOf('>');
                                         conceptName=conceptName.substring(k+1,j);
                                         //System.out.println(conceptName);
                                         return(new Concept(conceptName));
                                } catch (NullPointerException e) {
                                         System.err.println("parameter doesn't have a physical concept");
                                         return null;

                                } catch (NoSuchElementException e) {
                                         System.err.println("parameter doesn't have a physical concept");
                                         return null;
                                }
                }
         /**
	 * fetch out the concept computed by the formula
	 * @param formula: formula name
	 * @return the Concept object is being computed by the formula
	 */
 
            public Concept getComputeConcept(String formula) {

                            String conceptName;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                    "SELECT ?concept " +
                                                    "WHERE {" +
                                                    "    base:"+formula+" base:computes_concept ?concept . " +
                                                    "      }";
                               try {    
                                        ResultSet results=sparqlQueryExecution(queryString);
                                        QuerySolution soln = results.next() ;
                                        //conver to proper formula string
                                        conceptName=soln.toString();
                                        int k=conceptName.indexOf('#');
                                        int j=conceptName.lastIndexOf('>');
                                        conceptName=conceptName.substring(k+1,j);
                                        //System.out.println(conceptName);
                                        return(new Concept(conceptName));
                               } catch (NullPointerException e) {
                                         System.err.println("concpet computed by the formula is not given");
                                         return null;

                               } catch (NoSuchElementException e) {
                                         System.err.println("concpet computed by the formula is not given");
                                         return null;
                               }
                }
            /**
	 * fetch out the unit of the function input parameter
	 * @param input: parameter name
	 * @return the unit object of the parameter
	 */
            public  Unit getUnits(String input) {               

                            String unit;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                    "PREFIX  reprSciUnits: <http://sweet.jpl.nasa.gov/2.2/reprSciUnits.owl#>"+
                                                    "SELECT ?units " +
                                                    "WHERE {" +
                                                    "    base:"+input+" reprSciUnits:hasUnit ?units . " +
                                                    "      }";
                                try {    
                                         ResultSet results=sparqlQueryExecution(queryString);
                                         //ResultSetFormatter.out(System.out, results, query);
                                         QuerySolution soln = results.next() ;
                                         //conver to proper formula string
                                         unit=soln.toString();
                                         int k=unit.indexOf('#');
                                         int j=unit.lastIndexOf('>');
                                         unit=unit.substring(k+1,j);
                                         return new Unit(unit);
                                } catch (NullPointerException e) {
                                         System.err.println("parameter doesn't have a unit");
                                         return null;

                                } catch (NoSuchElementException e) {
                                         System.err.println("parameter doesn't have a unit");
                                         return null;
                                }
                }
            
             public Parameter getParameterFromConcept(Concept concept) {               
                            String parameterName;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                    "PREFIX  reprSciUnits: <http://sweet.jpl.nasa.gov/2.2/reprSciUnits.owl#>"+
                                                    "SELECT ?parameter " +
                                                    "WHERE {" +
                                                    "    ?parameter  base:physical_concept base:"+concept.getConceptName()+"."+
                                                    "      }";
                                try {    
                                         ResultSet results=sparqlQueryExecution(queryString);
                                         //ResultSetFormatter.out(System.out, results, query);
                                         QuerySolution soln = results.next() ;
                                         //conver to proper formula string
                                         parameterName=soln.toString();
                                         int k=parameterName.indexOf('#');
                                         int j=parameterName.lastIndexOf('>');
                                         parameterName=parameterName.substring(k+1,j);
                                         return  new Parameter(
                                                 parameterName,
                                                 getUnits(
                                                 parameterName),
                                                 getPhysicalConcept(
                                                 parameterName),
                                                 getSymbol(
                                                 parameterName));
                                } catch (NullPointerException e) {
                                         System.err.println("given concept is not the physical concpet of any parameter");
                                         return null;

                                } catch (NoSuchElementException e) {
                                         System.err.println("given concept is not the physical concpet of any parameter");
                                         return null;
                                }
                }

            
            
                /**
	 * return a string having detailed information of the Concept object
	 * @param physical_concept: a Concept object 
	 * @return the string containing detailed information 
	 */
   
            public  String exact_string(Concept physical_concept) {
                            ArrayList<Formula> functions=getFormulaList(physical_concept);
                            ArrayList<Parameter> parameter;
                            String value ="";
                            for(int i=0;i<functions.size();i++) {
                                    parameter=functions.get(i).getformulaInputParameters();
                                    value=value+functions.get(i).getformulaName()+",";
                                    for(int k=0;k<parameter.size();k++) {
                                            value=value+parameter.get(k).getParameterName()+",";
                                            value=value+parameter.get(k).getParameterConcept()
                                                    .getConceptName()+",";
                                            value=value+parameter.get(k).getParameterUnit().getUnitName()+",";
                                            value=value+parameter.get(k).getParameterSymbol();
                                            if(i==functions.size()-1 && k==parameter.size()-1) {
                                                break;
                                            }
                                            else
                                            value=value+",";
                                    }
                            }
                      
                        return "<< "+physical_concept.getConceptName()+","+value+" >>";


            }
             /**
	 * This method is called to calculate value of a concept when each formula linked with a 
	 * concept is having same units or everything to be choose from HashMap is available 
	 * @param physical_concept: a Concept object value of which needs to be determined 
	 * @return the HashMap having Formula objects as key and its result as value
         * @throws : UnknownFunctionException ,UnparsableExpressionException
	 */
      
            public  HashMap computeConceptValue(Concept physical_concept) throws UnknownFunctionException, UnparsableExpressionException {
                            HashMap<Formula,Double> conceptValue=new HashMap<Formula,Double>();
                            ArrayList<Parameter> input_values;
                            double result;
                            ArrayList<Formula> functions=getFormulaList(physical_concept);
                            for(int i=0;i<functions.size();i++) {
                                    input_values=functions.get(i).getformulaInputParameters();
                                    ExpressionBuilder builder=new ExpressionBuilder(
                                            functions.get(i).getmathematicalExpressions()
                                            .replace("toThePower","^"));
                                    for(int k=0;k<input_values.size();k++) {
                                            builder=builder.withVariable(
                                                    input_values.get(k).getParameterSymbol()
                                                    ,parameterValueHashMap.get(
                                                    (input_values.get(k))).getMeasurementValue());
                                    }
                                    Calculable calc=builder.build();
                                    result= calc.calculate();
                                    conceptValue.put(functions.get(i), (double)result);
                                    // System.out.println(functions.get(i).getformulaName()+"->"+result);

                            }

                           return conceptValue;


            }
            /**
	 * this method is called to find average
             * of values stored in the HashMap
	 * @param dataPerServiceMap: a HashMap containing the measurement for each service address
	 * @return the average
	 */

            public<T> Double average(HashMap<T,FinalResult> value) {

                            if(value.isEmpty()) {
                                return (Double)null;
                            }
                            double conceptValuel=0;
                            for (Iterator<T> it = value.keySet().iterator(); it.hasNext();) {
                                T key = it.next();
                                try {
                                    FinalResult result =value.get(key);
                                    conceptValuel=conceptValuel+result.getResultValue();

                                }
                                //if there is a null pointer exception => there must be null value
                                //stored in the HashMap
                                catch(NullPointerException e) {
                                    return null;
                                }
                               

                            }

                    return conceptValuel/value.size();
            }
                /**
	 * This method is called after getting transition function which governs unit conversion from one unit
         * to other
	 * @param transitionFunction: a mathematical relation between the units,got by transitionFunction(String firstUnit,String secondUnit)
	 * @param unit: unit stands for the unit object which needs to be convert
	 * @param secondUnitValue: value in the unit which needs to be converted
	 * @return the result after conversion in desired unit
         * @throws : UnknownFunctionException ,UnparsableExpressionException
	 */
     
            public Double convertUnit(String transitionFunction,
                    Unit unit,
                    double secondUnitValue) throws UnknownFunctionException, UnparsableExpressionException {           
                            //System.out.println(transitionFunction);
                          try {  ExpressionBuilder builder=new ExpressionBuilder(
                                    transitionFunction).withVariable(
                                    unit.getUnitName(),
                                     secondUnitValue);
                            Calculable calc=builder.build();
                            return (calc.calculate());
                          } catch(NullPointerException e) {
                              System.out.println(e.getMessage()+" conversion not defined ");
                              return null;
                          }

            }

             /**
         * This function internally call two function namely convertUnit() & transitionFunction()
         * to accomplish the unit conversion in single step
	 * @param firstUnit:the unit to which we need to convert
	 * @param secondUnit: the unit from which we needs to convert
	 * @param secondUnitValue: value in the unit which needs to be converted
	 * @return the result after conversion in desired unit
         * @throws : UnknownFunctionException ,UnparsableExpressionException
	 */

         @Override
            public Double singleStepConvertUnit(Unit firstUnit,Unit secondUnit,
                    double secondUnitValue) throws UnknownFunctionException, UnparsableExpressionException {

                 return convertUnit(transitionFunction(firstUnit,secondUnit),secondUnit,secondUnitValue) ;

            }
                /**
	 * this function is called to get a transition function between two units,working well for Scaling,
         * shifting,power type units,also work fine for base to derived unit conversion or derived to base unit
	 * @param firstUnit: unit object in which we need to convert
	 * @param secondUnit:unit object from which we need to convert
	 * @return the transition function which only needs to replace variables for evaluation (which is done in convertUnit())
	 */
     
            public  String transitionFunction(Unit firstUnit,Unit secondUnit) {

                            if(firstUnit.getUnitName().equals(secondUnit.getUnitName())) {
                                return(secondUnit.getUnitName());
                            }
                            int flag=0;//to make sure the base unit is either pure scaling or what?in case of pure 
                                       //scaling i will replace scaling factor with 1/scaling factor
                            boolean inverse = false;//to make sure either [:firstUnit,:hasBaseUnit,:secondUnit]
                                                    //or [:firstUnit,:hasBaseUnit,:secondUnit] ,in the reverse case scaling 
                                                    //factor need to be replaced by division and shifting factor 
                                                    //need to be replace with subtraction
                            String shiftFactor;//shiftingNumber value
                            String scalingFactor;//scalingNumber value
                            String power;//to break down such type of expressions like 1.e2 from meter to centimeter
                            String multiplier;
                            String queryString ="PREFIX base: <http://www.semanticweb.org/ontologies/2011/7/S&A.owl#>"+
                                                "PREFIX reprSciUnits: <http://sweet.jpl.nasa.gov/2.2/reprSciUnits.owl#>"+
                                                "PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
                                                "PREFIX owl:  <http://www.w3.org/2002/07/owl#>"+
                                                "PREFIX xsd:  <http://www.w3.org/2001/XMLSchema#>"+
                                                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"+
                                                "PREFIX oper2:<http://sweet.jpl.nasa.gov/2.2/reprMathOperation.owl#>"+

                                                "SELECT ?shift ?scale ?p ?type ?powerOfUnit " +
                                                "WHERE { " +
                                                "   {?p reprSciUnits:hasBaseUnit ?q.}" +
                                                "   FILTER ( (?p = reprSciUnits:"+firstUnit.getUnitName()+" && ?q = reprSciUnits:"+secondUnit.getUnitName()+")|| (?q = reprSciUnits:"+firstUnit.getUnitName()+" && ?p = reprSciUnits:"+secondUnit.getUnitName()+" )) "+
                                                "   OPTIONAL {?p reprSciUnits:hasShiftingNumber ?shift  .}   "+
                                                "   OPTIONAL {?p reprSciUnits:hasScalingNumber ?scale  .}   "+
                                                "   OPTIONAL {?p rdf:type ?type  .}   "+
                                            //  "   OPTIONAL {?p oper2:toThePower ?powerOfUnit  .}   "+
                                                "     }";

                            ResultSet results=sparqlQueryExecution(queryString);
                            try {    
                                        QuerySolution soln = results.next() ;
                                        //convert to proper formula string
                                        Object shift=soln.get("shift");
                                        Object scale=soln.get("scale");
                                        String inverseTransition=soln.get("p").toString();
                                        String unitClassType=soln.get("type").toString();
                                        //to check which pair exist
                                        if(inverseTransition.substring(inverseTransition.indexOf('#')+1)
                                                .equals(secondUnit.getUnitName()))
                                        inverse=true;
                                        //to know either this is pure scaling
                                        if(unitClassType.substring(unitClassType.indexOf('#')+1)
                                                .equals("UnitDerivedByScaling"))
                                        flag=1;
                                        //this is to dealt with UnitDerivedByRaisingToPower type
                                        /*if(unitClassType.substring(unitClassType.indexOf('#')+1).equals("UnitDerivedByRaisingToPower"))
                                        {   if(inverse==false)
                                            return "("+secondUnit+"^"+soln.get("powerOfUnit")+")";
                                        else
                                            return "("+secondUnit+"^(1/"+soln.get("powerOfUnit")+"))";
                                        }*/        
                                        //it needs to be since if a conversion only have pure multiplication type then 
                                        //shiftfactor should be 0
                                        if(shift==null)
                                        shiftFactor="0";

                                        else {
                                                shiftFactor=shift.toString();
                                                int i=shiftFactor.indexOf('^');
                                                shiftFactor=shiftFactor.substring(0,i);
                                        }
                                        if(scale==null)//it needs to be since if a conversion only have pure shifting type 
                                                    //then shiftfactor should be 0
                                        scalingFactor="1";

                                        else {

                                                scalingFactor=scale.toString();
                                                int i=scalingFactor.indexOf('^');

                                                    if(flag==1)//if pure scaling type conversion
                                                    scalingFactor="(1/"+scalingFactor.substring(0,i)+")";

                                                    else
                                                    scalingFactor=scalingFactor.substring(0,i);

                                                    if(scalingFactor.indexOf(".E")>0||scalingFactor.indexOf(".e")>0) {

                                                        i=scalingFactor.indexOf('.');
                                                        multiplier=scalingFactor.substring(0,i);
                                                        power=scalingFactor.substring(i+2);

                                                            if(flag==1)
                                                            scalingFactor="("+multiplier+"*(10^-"+power+"))";

                                                            else 
                                                            scalingFactor="("+multiplier+"*(10^"+power+"))";
                                                    }

                                        }
                                        if(inverse==true) {    
                                            //System.out.println("("+secondUnit+"-"+shiftFactor+")/"+scalingFactor);
                                            return  "("+secondUnit.getUnitName()+"-"+shiftFactor+")/"+scalingFactor;

                                        }
                                        else {
                                            //System.out.println(shiftFactor+"+"+scalingFactor+"*"+secondUnit);
                                            return (shiftFactor+"+"+scalingFactor+"*"+secondUnit.getUnitName());
                                        }
                           } catch (NoSuchElementException e) {
                                           System.out.println(e.getMessage()+" conversion functions can't handle this");
                                           return null;
                           } catch (NullPointerException e) {
                                           System.out.println(e.getMessage()+" conversion functions can't handle this");
                                           return null;
                           }
            }             

                        /**
	 * this is called to execute Sparql queries
	 * @param Query: a query to be executed
	 * @return the ResultSet of the passed query results.
	 */
            public ResultSet sparqlQueryExecution(String Query) {
                                com.hp.hpl.jena.query.Query query = QueryFactory.create(Query);
                                // Execute the query and obtain results
                                QueryExecution qe = QueryExecutionFactory.create(query, model);
                                ResultSet results=qe.execSelect();
                                //ResultSetFormatter.out(System.out, results, query);
                                return(results);

            }

//                  /**
//	 * This method is called to calculate value of a concept depending upon what data
//         * is available in HashMap.
//	 * @param physical_concept: a Concept object value of which needs to be determined
//	 * @return the HashMap having Formula objects as key and its result as value
//         * @throws : UnknownFunctionException ,UnparsableExpressionException
//	 */
//
//            public  HashMap<Formula,Double> expandAndCompute(Concept physical_concept) throws UnknownFunctionException, UnparsableExpressionException {
//                            ArrayList<Formula> functions=getFormulaList(physical_concept);
//                            HashMap<Formula,Double> conceptComputeValue=new HashMap<Formula,Double>();//to put formula object as key and result as value
//                            HashMap<Formula,Double> recieveHaspMap;
//                            ArrayList<Parameter> inputParam;
//                            ArrayList<Parameter> foundParam;
//                            double parameterValue;
//                            double formulaResult;
//                            for(int i=0;i<functions.size();i++) {
//                                     inputParam=functions.get(i).getformulaInputParameters();
//                                     ExpressionBuilder builder=new ExpressionBuilder(
//                                             functions.get(i).getmathematicalExpressions()
//                                             .replace("toThePower","^"));
//                                     for(int j=0;j<inputParam.size();j++) {
//
//                                              foundParam=getParameterValue(inputParam.get(j));
//                                              if(foundParam==null) {
//                                                        recieveHaspMap=expandAndCompute(
//                                                                inputParam.get(j).getParameterConcept());
//                                                        Iterator<Formula>  iterator=recieveHaspMap.keySet().iterator();
//                                                        Formula formula=iterator.next();
//                                                        parameterValue=recieveHaspMap.get(formula);
//                                                        parameterValue=convertUnit(
//                                                                transitionFunction(
//                                                                inputParam.get(j).getParameterUnit(),
//                                                                formula.getoutputUnit()),
//                                                                formula.getoutputUnit(),
//                                                                parameterValue);
//                                                        //System.out.println("re"+parameterValue+" "+inputParam.get(j).getParameterUnit()+" "+inputParam.get(j).getParameterName());
//                                              }
//                                              else {
//                                                        //System.out.println("without recursion"+inputParam.get(j).getParameterName()+inputParam.get(j).getParameterUnit()+foundParam.getParameterUnit()+foundParam.getParameterName());
//                                                        parameterValue=desiredParameterForm(foundParam,inputParam.get(j).getParameterUnit()).getMeasurementValue();
//                                                        //System.out.println(parameterValue+" "+inputParam.get(j).getParameterUnit()+" "+inputParam.get(j).getParameterName());
//                                              }
//                                                    builder=builder.withVariable(
//                                                            inputParam.get(j).getParameterSymbol(),
//                                                            parameterValue);
//                                      }
//                                      Calculable calc=builder.build();
//                                      formulaResult=calc.calculate();
//                                      conceptComputeValue.put(functions.get(i), (double)formulaResult);
//                            }
//
//                           return conceptComputeValue;
//
//
//            }
//                      
    
         /**
	 * This method is called to know what data is available in HashMap.first it tries for the key 
         * having the same name as the name of passed Parameter object and then having the same concept
	 * @param parameter:a Parameter object
	 * @return the Parameter object from the HashMap sharing the same name or concept with 
         * passed parameter object,if no data is found then it returns null.
	 */
      
                
           public  ArrayList<Parameter> getParameterValue(Parameter parameter) {
                            ArrayList<Parameter> sameConceptParameter=new ArrayList();
                            boolean sameConceptParam=false;
                            for (Iterator<Parameter> it = getHashMap().keySet().iterator(); it.hasNext();) {
                                Parameter key = it.next();
                                    
                                    if(key.getParameterConcept().getConceptName()
                                            .equals(parameter.getParameterConcept().getConceptName())) {
                                        sameConceptParam=true;
                                        sameConceptParameter.add(key);
                                    }

                            }
                            if(sameConceptParam==true) {
                                return sameConceptParameter;
                            }
                       
                            return null;
            }
            
         /**
	 * This method is called to fetch list of parameters linked with the same concept as that
         * of the passed parameter object
	 * @param parameter:a Parameter object
         * @param parameterValues:parameter value HashMap
	 * @return the Parameter object from the HashMap sharing the same name or concept with 
         * passed parameter object,if no data is found then it returns null.
	 */
      
                
            public  ArrayList<Parameter> getParameterValue(Concept concpet,HashMap<Parameter,Measurement> parameterValues) {
                            parameterValueHashMap=parameterValues;
                            ArrayList<Parameter> sameConceptParameter=new ArrayList();
                            boolean sameConceptParam=false;
                            for (Iterator<Parameter> it = getHashMap().keySet().iterator(); it.hasNext();) {
                                Parameter key = it.next();
                                    
                                    if(key.getParameterConcept().getConceptName()
                                            .equals(concpet.getConceptName())) {
                                        sameConceptParam=true;
                                        sameConceptParameter.add(key);
                                    }

                            }
                            if(sameConceptParam==true) {
                                return sameConceptParameter;
                            }
                       
                            return null;
            }

         /**
         * this is called from the Query Composition Manager(computeQueryConcept() method)
         * This is to dealt with the direct measurement of the concept as a native service
         * @param foundParam:a ArrayList of similar(same name or concept)parameter objects available in the HashMap
	 * @return the HashMap containing Formula(having first available unit) & value after conversions
	 */


            public HashMap<Formula,Measurement> virtualFormula(ArrayList<Parameter> foundParam)
                    throws UnknownFunctionException, UnparsableExpressionException {
                HashMap<Formula,Measurement> formulaValue=new HashMap();
                //convert each of the found parameter value in the unit of first found parameter
                for(int i=0;i<foundParam.size();i++) {
                    formulaValue.put(new Formula(foundParam.get(i).getParameterUnit(),foundParam.get(i).getParameterConcept()),this.parameterValueHashMap.get(foundParam.get(i)));

                }
                return formulaValue;

            }
            
         /**
	 * This method is called to get the parameter value in the desired unit from the HashMap
	 * @param foundParam:a ArrayList of similar(same name or concept)parameter objects available in the HashMap
         * @param unit required unit
         * @param concept: concept of the parameters
         * @param location: it is the location of interest for measurement
         * @param fusionFunction: type of fusionFunction to be used
 	 * @return the average value of the parameter from all conversions
	 */
      
            
            public Measurement desiredParameterForm(ArrayList<Parameter> foundParam,Unit unit,
                    Concept concept
                    ,Location location,
                    String fusionFunction
                    )
                    throws UnknownFunctionException, UnparsableExpressionException {
                Double valueHolder=0.0;
                int j=0;
                ArrayList<Measurement> measurements=new ArrayList() ;
                for(int i=0;i<foundParam.size();i++) {
                    valueHolder=convertUnit(
                           transitionFunction(
                           unit,
                           foundParam.get(i).getParameterUnit()),
                           foundParam.get(i).getParameterUnit(),
                           getValuesFromHashMap(foundParam.get(i)).getMeasurementValue());
                    //System.out.println(valueHolder+valueHolder ==(Double)null);
                    if(valueHolder ==(Double)null) {
                        continue;
                    }
                    j++;
                    measurements.add(new Measurement(valueHolder,getValuesFromHashMap(foundParam.get(i)).getMeasurementLocation()));
                    //parameterValue=parameterValue+valueHolder;
                    
                }
               if(j==0) {
                   return null;
               }
                df=new DataFusionGeneratorImpl();
                return new Measurement(df.generateFinalDataValues(measurements,concept,location,fusionFunction),location);
                
            }
                    /**
	 * This method is called to access HashMap
	 * @param key:key of the HashMap value of which need to be determined
	 * @return the value from HashMap corresponding to that key
	 */
            public Measurement getValuesFromHashMap(Parameter key) {
                        
                      return parameterValueHashMap.get(key);
            }
            //to acees hashmap
            public HashMap<Parameter,Measurement> getHashMap() {
                        
                      return parameterValueHashMap;
            }
                     
         /**
         *
         * IN THIS METHOD WE WILL STOP EVALUATING DATAFLOW GRAPH WHENEVER WE HAVE ENOUGH PARAMETERS FOR
         * COMPUTING THE CONCEPT
         * THIS IS FAST BUT LESS ACCURACTE AS WE DIDN'T  CONSIDER THE CHILD VALUES FOR COMPUTING THE CONCEPT
         *
         *
	 * This method is called by the Query object to calculate value of a concept depending upon what data
         * is available in HashMap.
	 * @param subQueryList: ArrayList of SubQuery objects needed to calculate concept
         * @param parameterValues:HashMap of Parameter values
	 * @return the HashMap having Formula objects as key and its result as value
         * @throws : UnknownFunctionException ,UnparsableExpressionException
	 */
//
//            public HashMap<Formula,Double> computeConcept(ArrayList<SubQueryGeneratorImpl> subQueryList,
//                    HashMap<Parameter,Double> parameterValues,String fusionFunction) throws UnknownFunctionException, UnparsableExpressionException {
//
//                 parameterValueHashMap=parameterValues;
//                 ArrayList<Unit> unitList=new ArrayList();
//                 ArrayList<Parameter> foundParam;
//                 SubQueryGeneratorImpl key;
//                 boolean solvable=false;
//                 double parameterValue;//hold the value of parameter of subQuery
//                 double formulaResult;
//                 HashMap<Formula,Double> conceptComputeValue=new HashMap<Formula,Double>();
//                 HashMap<Formula,Double> recieveHashMap;
//                 ArrayList<SubQueryGeneratorImpl> subQueryOfConcept=new ArrayList();
//
//                 main:for(int i=0;i<subQueryList.size();i++) {
//                    ExpressionBuilder builder=new ExpressionBuilder(
//                                             subQueryList.get(i).getSubQueryFormula().getmathematicalExpressions().
//                                             replace("toThePower","^"));
//                    for(int j=0;j<subQueryList.get(i).getParametersToMeasure().size();j++) {
//                         //to check availability in HashMap
//                         foundParam=getParameterValue(
//                                 subQueryList.get(i).getParametersToMeasure().get(j));
//                         parameterValue=desiredParameterForm(
//                                     foundParam,subQueryList.get(i).getParametersToMeasure().get(j).getParameterUnit());
//
//                        //if not find any similar parameter
//                         if(parameterValue==(Double)null ) {
//                              if(subQueryList.get(i).furtherSubQuery==false) {
//                                continue main;
//
//                              }
//                             solvable=true;
//                             for(Iterator<SubQueryGeneratorImpl> it = subQueryList.get(i).getSubQueryHashMap().keySet().iterator();it.hasNext();) {
//                                key=it.next();
//                                if(key.getSubQueryNumber()==j) {
//                                    subQueryOfConcept.add(key);
//                                }
//                             }
//                             recieveHashMap=computeConcept(
//                                               subQueryOfConcept,
//                                               parameterValues
//                                               ,fusionFunction);
//                             unitList.add(subQueryList.get(i)
//                                     .getParametersToMeasure().get(j)
//                                     .getParameterUnit());
//                             parameterValue=finalConversion(
//                                     recieveHashMap,
//                                     unitList
//                                     ,fusionFunction).getResultValue();
//                             unitList.removeAll(unitList);//to be used further,otherwise some extra
//                                                          //elements will be there in the required conversion unit list
//
//                         }
//                         else {
//                             solvable=true;
//
//                         }
//                         builder=builder.withVariable(
//                                subQueryList.get(i).getParametersToMeasure().get(j).getParameterSymbol(),
//                                parameterValue);
//                         subQueryOfConcept.removeAll(subQueryOfConcept);
//
//                    }
//                    Calculable calc=builder.build();
//                    formulaResult=calc.calculate();
//                    conceptComputeValue.put(subQueryList.get(i).getSubQueryFormula(), (double)formulaResult);
//                }
//                if(solvable==false) {
//                    System.out.println("not enough data in the Hashmap");
//
//                }
//                return conceptComputeValue;
//            }


          /**
         * THIS FUNCTION ALSO CALLED DataFusion Class
         * THIS FUNCTION IS RESPONSIBLE FOR DEFINING FUSION FUNCTIONS IN THE QUERY LIKE FIRST,AVERAGE ETC
         * called by computeQueryConcept() in query.Query.java & computeConcept() in Sparql.java
	 * TO finally convert the computed concept in the desired unit(set by Setter object)
	 * @param formulaValues: HashMap having formula as key and concept value as
         * value(we get this from computeConcept()
         * from sparqlfunctions.Sparql.java)
         * @param unitList: the list of units in which the final value of Concept should be
         * @param fusionFunction: types of fusion function induced
         * @param concept:concept for which we are evaluating final value ,it is for passing to
         * DataFusion class
         * @param locationOfInterest:location at which we want our concept to be measured,it is for
         * passing to DataFusion class
         * @return: return value of Concept in required unit
         * @throws: UnknownFunctionException ,UnparsableExpressionException
            */

        public FinalResult finalConversion(HashMap<Formula,Measurement> formulaValues,
                ArrayList<Unit> unitList,String fusionFunction,Concept concept,Location locationOfInterest)
                throws UnknownFunctionException, UnparsableExpressionException {

            df=new DataFusionGeneratorImpl();
            Double convertValue=0.0,valueHolder;
            int i=0;
            ArrayList<Measurement> measurements=new ArrayList();
            Formula firstFormula;
            try {
                if(unitList.isEmpty()) {//return first value stored in HashMap since no unit is defined
                    System.err.println("NO UNIT WAS REQUESTED");
                    Iterator<Formula> it = formulaValues.keySet().iterator();
                    firstFormula=it.next();//take first one in all available units
                    return new FinalResult(firstFormula.getoutputUnit(),formulaValues.get(firstFormula).getMeasurementValue());
                }

                    main:for(int j=0;j<unitList.size();j++) {
                            for(Iterator<Formula> it = formulaValues.keySet().iterator();it.hasNext();) {
                                     Formula formula=it.next();
                                     valueHolder=convertUnit(
                                             transitionFunction(
                                             unitList.get(j),formula.getoutputUnit())
                                             ,formula.getoutputUnit(),
                                             formulaValues.get(formula).getMeasurementValue());
                                     if(valueHolder==(Double)null) {//if the converison is undefined
                                         continue;
                                     }
                                     else {
                                         i++;
                                         measurements.add(new Measurement(valueHolder,formulaValues.get(formula).getMeasurementLocation()));
                                         convertValue=convertValue+valueHolder;
                                     }
                            }
                            if(i==0) {//if no avilable unit can be converted to Required unit,
                                      //check for other conversion
                                continue main;
                            }
                            else {//if there are some convertible units

                                //call the DataFusion class for accessing fusion function definations
                                return new FinalResult(unitList.get(j),
                                        df.generateFinalDataValues(
                                                    measurements,concept,locationOfInterest,fusionFunction));
                            }
                    }
                    measurements.removeAll(measurements);
                    if(i==0) {//if not possible to convert to any desirable unit(ArrayList of allowed unit for concept)
                                 Iterator<Formula> it = formulaValues.keySet().iterator();
                                 firstFormula=it.next();//take first one in all available units
                                 for(Iterator<Formula> it2 = formulaValues.keySet().iterator();it2.hasNext();) {
                                    Formula formula=it2.next();
                                    valueHolder=convertUnit(
                                            transitionFunction(
                                            firstFormula.getoutputUnit(),formula.getoutputUnit())
                                            ,formula.getoutputUnit(),
                                            formulaValues.get(formula).getMeasurementValue());
                                    if(valueHolder==(Double)null) { //if the conversion to first available unit is undefined
                                        continue;
                                    }
                                    else {
                                        i++;
                                        measurements.add(new Measurement(valueHolder,formulaValues.get(formula).getMeasurementLocation()));
                                       // convertValue=convertValue+valueHolder;
                                    }
                                }
                                if(i==1) {//if no available unit can be convert to first available unit
                                   System.err.println("RETURNED UNIT IS DIFFERENT FROM REQUESTED UNIT");
                                   return new FinalResult(firstFormula.getoutputUnit(),formulaValues.get(firstFormula).getMeasurementValue());//return the first value
                                }
                                else {
                                   System.err.println("RETURNED UNIT IS DIFFERENT FROM REQUESTED UNIT");
                                   return new FinalResult(firstFormula.getoutputUnit(),
                                            df.generateFinalDataValues(
                                                    measurements,concept,locationOfInterest,fusionFunction));

                                }
                        
                   }
            return new FinalResult((Double)null);
           }catch(NoSuchElementException e) {
                    System.out.println(e.getMessage()+" Concept value can't be evaluated:element doesn't exist");
                    return new FinalResult((Double)null);
           } catch(NullPointerException e) {
                    System.out.println(e.getMessage()+" Concept value can't be evaluated:null reference");
                    return new FinalResult((Double)null);
           }


        }

         /**
	 * This method is called by the Query object to calculate value of a concept depending upon what data
         * is available in HashMap.
	 * @param subQueryList: ArrayList of SubQuery objects needed to calculate concept
         * @param parameterValues:HashMap of Parameter values
	 * @return the HashMap having Formula objects as key and its result as value
         * @throws : UnknownFunctionException ,UnparsableExpressionException
	 */

            public HashMap<Formula,Measurement> computeTillExpend(ArrayList<SubQueryGeneratorImpl> subQueryList,
                    HashMap<Parameter,Measurement> parameterValues,
                    String fusionFunction) throws UnknownFunctionException
                        , UnparsableExpressionException {

                 parameterValueHashMap=parameterValues;
                 ArrayList<Parameter> foundParam=new ArrayList();//hold the lis of similar parameters after searching in Hashmap
                 ArrayList<Unit> unitList=new ArrayList();
                 SubQueryGeneratorImpl key;
                 boolean solvable=false;//it will denote that by any means(recursion or all parameters found in HashMap)
                 Double parameterValue,parameterValue1=(Double)null,parameterValue2=(Double)null;//hold the value of parameter of subQuery
                 //parameterValue1 holds value of a parameter obtained by using recursion
                 //parameterValue2 holds value of parameter by direct searching in the hashmap for similar parameters
                 //parametervalue hold mean of two(with existance condition of each)
                 double formulaResult;
                 HashMap<Formula,Measurement> conceptComputeValue=new HashMap<Formula,Measurement>();
                 HashMap<Formula,Measurement> recieveHashMap=new HashMap();//used in recursion to accept
                 ArrayList<SubQueryGeneratorImpl> subQueryOfConcept=new ArrayList();
                 main:for(int i=0;i<subQueryList.size();i++) {//iterate over subQueries
                     ExpressionBuilder builder=new ExpressionBuilder(
                                             subQueryList.get(i).getSubQueryFormula()
                                             .getmathematicalExpressions().
                                             replace("toThePower","^"));
                    //iterate over all parameters of the Sub Query
                    sub:for(int j=0;j<subQueryList.get(i).getParametersToMeasure().size();j++) {
                         parameterValue1=(Double)null;parameterValue2=(Double)null;
                         solvable=false;//start with the final condition
                         //to check availability in HashMap
                         foundParam=getParameterValue(
                                 subQueryList.get(i).getParametersToMeasure().get(j));
                         if(subQueryList.get(i).furtherSubQuery==true) {
                                     //if the Cncept linked with the parameter can be expend
                                     for(Iterator<SubQueryGeneratorImpl> it = subQueryList.get(i)
                                             .getSubQueryHashMap().keySet().iterator();it.hasNext();) {
                                        key=it.next();
                                        if(key.getSubQueryNumber()==j) {
                                            subQueryOfConcept.add(key);
                                        }
                                     }
                                     recieveHashMap=computeTillExpend(
                                                       subQueryOfConcept,
                                                       parameterValues,fusionFunction);
                                     if(!recieveHashMap.isEmpty()) {//if there is result form recursion

                                                 solvable=true;//from here we can say there
                                                               //must be a value possible by recursion
                                                 unitList.add(subQueryList.get(i)
                                                         .getParametersToMeasure().get(j)
                                                         .getParameterUnit());
                                                 parameterValue1=finalConversion(
                                                         recieveHashMap,
                                                         unitList,
                                                         fusionFunction,
                                                         subQueryList.get(i).getParametersToMeasure().get(j).getParameterConcept(),
                                                          subQueryList.get(i).getLocationOfInterest()
                                                         ).getResultValue();
                                                 unitList.removeAll(unitList);//empty the ArrayList of unit to
                                                                              //be able to use in n ext iteration(step)

                                     }
                                     else {//if no result from recursion
                                           //if we didn't find any similar parameterand also can not be expend to get
                                           //any fruitful result then simply we cant get the value of parameter
                                           //so let us escape to new SubQuery since present Sub Query cant serve the
                                           //purpose as the parameter is not able to be calculated
                                         if(foundParam==null)
                                         break sub;
                                     }

                         }
                          if(foundParam!=null) {
                                    solvable=true;
                                    parameterValue2=desiredParameterForm(
                                             foundParam,subQueryList.get(i)
                                             .getParametersToMeasure().get(j)
                                             .getParameterUnit(),
                                              subQueryList.get(i).getParametersToMeasure().get(j).getParameterConcept(),
                                              subQueryList.get(i).getLocationOfInterest(),
                                             fusionFunction
                                             ).getMeasurementValue();

                          }
                          else {
                                    if(subQueryList.get(i).furtherSubQuery==false || recieveHashMap.isEmpty()) {
                                    solvable=false;
                                    continue main;

                                }
                          }
                          if(parameterValue1!=null && parameterValue2!=null) {//if both value are presnt
                                     parameterValue=(parameterValue1+parameterValue2)/2;
                          }
                          else {
                                    if(parameterValue1==null && parameterValue2==null) {
                                        solvable=false;
                                       // continue main;
                                    }
                                    if(parameterValue1!=null) //only we get result by expansion
                                    parameterValue=parameterValue1;

                                    else                      //only we get result by direct search in the Parameter value HashMap
                                    parameterValue=parameterValue2;

                          }
                          
                          
                         builder=builder.withVariable(
                                     subQueryList.get(i).getParametersToMeasure().get(j)
                                        .getParameterSymbol(),
                                     parameterValue);
                                     subQueryOfConcept.removeAll(subQueryOfConcept);

                    } //end of sub loop
                    if(solvable==true) {
                        
                                     Calculable calc=builder.build();
                                     formulaResult=calc.calculate();
                                     conceptComputeValue.put(new Formula(subQueryList.get(i)
                                                     .getSubQueryFormula().getformulaName(),
                                                     subQueryList.get(i)
                                                     .getSubQueryFormula().getoutputUnit()),
                                                     new Measurement((double)formulaResult,
                                                     subQueryList.get(i).getLocationOfInterest()));
                            
                        
                     }
                         
                 }//end of main loop
                
                if(solvable==false) {
                    System.out.println("not enough data in the Hashmap");

                }
                return conceptComputeValue;
            }

}