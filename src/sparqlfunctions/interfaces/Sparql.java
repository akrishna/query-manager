/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.interfaces;

import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import java.util.ArrayList;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Formula;
import sparqlfunctions.constants.Unit;

/**
 *
 * @author krishna
 */
public interface Sparql {


       /**
         * This function internally call two function namely convertUnit() & transitionFunction()
         * to accomplish the unit conversion in single step
	 * @param firstUnit:the unit to which we need to convert
	 * @param secondUnit: the unit from which we needs to convert
	 * @param secondUnitValue: value in the unit which needs to be converted
	 * @return the result after conversion in desired unit
         * @throws : UnknownFunctionException ,UnparsableExpressionException
	 */

            public Double singleStepConvertUnit(Unit firstUnit,Unit secondUnit,
                    double secondUnitValue) throws UnknownFunctionException, UnparsableExpressionException ;


       /**
	 * fetch out all the functions Formula object that can be used in evaluating physical_concept
	 * @param physical_concept: a Concept object for which functions need to be determined
	 * @return then ArrayList of Formula objects
	 */

            public  ArrayList<Formula> getFormulaList(Concept physical_concept);
 
}
