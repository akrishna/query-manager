/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.constants;



/**
 *
 * this class is used to hold the final result by computing a concept
 */
public class FinalResult {
     private Double resultValue;//value of the concept
     private Unit resultUnit;//unit of the concept that is being compute

     public FinalResult(Unit unit,double value) {
             resultValue=value;
             resultUnit=unit;
     }

     public FinalResult(Double value) {
             resultValue=value;
     }

     public double getResultValue() {
             return resultValue;
     }

     public Unit getResultUnit() {
             return resultUnit;
     }

     public void setResultValue(double value) {
             resultValue=value;
     }

     public void setResultUnit(Unit unit) {
             resultUnit=unit;
     }


}
