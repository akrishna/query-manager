/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sparqlfunctions.constants;

/**
 *
 * used to hold any parameter like temperature_degreeC
 */
public class Parameter {
    
      private String  parameterName;//name of the parameter
      private Unit  parameterUnit;//unit of the parameter
      private Concept parameterConcept;//physical concept of the parameter like temperature is
                                       //the concpet of temperature_degreeC
      private String  hasMathematicalType;
      private String parameterSymbol;//symbol of the parameter like W is the symbol of windspeed_MeterPerSecond
      
      
      public Parameter(String paraName) {
          parameterName=paraName;
          
      }
      
      public Parameter(String paraName,Concept concept) {
          parameterName=paraName;
          parameterConcept=concept;
          
      }
      
      public Parameter(String paraName,Unit paraUnit,Concept paraConcept,String paraSymbol) {
          parameterName=paraName;
          parameterUnit=paraUnit;
          parameterConcept=paraConcept;
          parameterSymbol=paraSymbol;
          
          
      }
      
      public String getParameterHasMathematicalType() {
                    return hasMathematicalType;
        }
      
      public String getParameterName() {
		return parameterName;
      }
      
      public Unit getParameterUnit() {
		return parameterUnit;
      }
      
      public Concept getParameterConcept() {
		return parameterConcept;
      }
      
      public String getParameterSymbol() {
		return parameterSymbol;
      }
      
      public void setParameterHasMathematicalType(String mathematicalType) {
                hasMathematicalType=mathematicalType;
      }
      
      public void setParameterName(String name) {
		parameterName = name;
      }
      
      public void setParameterUnit(Unit unit) {
		parameterUnit = unit;
      }
      
      public void setParameterConcept(Concept concept) {
		parameterConcept=concept;
      }
      
      public void setParameterSymbol(String symbol) {
		parameterSymbol = symbol;
      }
      
      public void printParameterContents() {
                System.out.println(parameterName);
                System.out.println(parameterUnit);
                System.out.println(parameterConcept.getConceptName()); 
                System.out.println(parameterSymbol);
                  
          
      }
      
      
      
    
    
}
