/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.constants;

import de.congrace.exp4j.Calculable;
import de.congrace.exp4j.ExpressionBuilder;
import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import sparqlfunctions.impl.SparqlImpl;


/**
 *
 * @author abc
 */
public class DecayFormula {


    public double computeDecay(double alpha,double limit,Concept concept ) throws UnknownFunctionException, UnparsableExpressionException {
            SparqlImpl sparql= new SparqlImpl();
            String formulaExpression=sparql.getDecayFormulaExpression(concept);
           try {
                 ExpressionBuilder builder=new ExpressionBuilder(formulaExpression);
                 builder=builder.withVariable("alpha", alpha).withVariable("limit",limit);
                 Calculable calc=builder.build();
                 return calc.calculate();
           }
           catch(NullPointerException e) {
               System.err.println("does not exist in ontology");
               return 0;

           }
        }
}
