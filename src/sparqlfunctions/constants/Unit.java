/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sparqlfunctions.constants;

/**
 *
 * used to hole unit of a parameter
 */
public class Unit {
    private String unitName;//name of the unit like degreeC is the unit of temperature_degreeC
    
    public Unit(String unitNameString) {
        unitName=unitNameString;
    }
    
    public String getUnitName() {
        return unitName;
    }
    
    public void setUnitName(String unitNameString) {
        unitName=unitNameString;
    }
}
