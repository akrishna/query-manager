/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.constants;



/**
 *
 * @author krishna
 */
public class IntervalOperations {


         /**
         * it gets call from Query object or from where we want to filter the find Services based on the desired
         * value set of accuracy and range
         * it used to find intersection of two intervals
	 * @param propertyInterval: it can be a Accuracy or a Range object
         * @return: it returns the intersection interval of all input interval(either Range or Accuracy)
	 */

  public  static<T> T intersection(T... propertyInterval) {

            String componentName=propertyInterval[0].getClass().toString();
            int j=componentName.lastIndexOf('.');
            //if it is a object of Accuracy class
            if(componentName.substring(j+1).equals("Accuracy")) {
                   Accuracy intersectionInterval=new Accuracy();
                   for(int i=0;i<propertyInterval.length;i++){
                       if(intersectionInterval.getLowerValueObject().getLowerBoundValue()> ((Accuracy)propertyInterval[i]).getUpperValueObject().getUpperBoundValue()
                               ||((Accuracy)propertyInterval[i]).getLowerValueObject().getLowerBoundValue()> intersectionInterval.getUpperValueObject().getUpperBoundValue()) {
                           return null;
                       }
                        intersectionInterval.setLowerValueObject(
                                new LowerBound(
                                max(
                                intersectionInterval.getLowerValueObject().getLowerBoundValue(),
                                ((Accuracy)propertyInterval[i]).getLowerValueObject().getLowerBoundValue())));
                         intersectionInterval.setUpperValueObject(
                                new UpperBound(
                                min(
                                intersectionInterval.getUpperValueObject().getUpperBoundValue(),
                                ((Accuracy)propertyInterval[i]).getUpperValueObject().getUpperBoundValue())));

                   }
                   return (T) intersectionInterval;
            }
            else {
                    Range intersectionInterval=new Range();
                    for(int i=0;i<propertyInterval.length;i++){
                        if(intersectionInterval.getLowerValueObject().getLowerBoundValue()> ((Range)propertyInterval[i]).getUpperValueObject().getUpperBoundValue()
                               ||((Range)propertyInterval[i]).getLowerValueObject().getLowerBoundValue()> intersectionInterval.getUpperValueObject().getUpperBoundValue()) {
                           return null;
                        }
                        intersectionInterval.setLowerValueObject(
                                new LowerBound(
                                max(
                                intersectionInterval.getLowerValueObject().getLowerBoundValue(),
                                ((Range)propertyInterval[i]).getLowerValueObject().getLowerBoundValue())));
                         intersectionInterval.setUpperValueObject(
                                new UpperBound(
                                min(
                                intersectionInterval.getUpperValueObject().getUpperBoundValue(),
                                ((Range)propertyInterval[i]).getUpperValueObject().getUpperBoundValue())));

                   }
             return (T) intersectionInterval;
            }

            



    }

    static double max(double value1,double value2) {

        if(value1>value2) {
            return value1;
        }
        else return value2;

    }

    static double min(double value1,double value2) {

        if(value1>value2) {
            return value2;
        }
        else return value1;

    }
}
