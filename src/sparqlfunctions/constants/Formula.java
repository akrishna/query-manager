/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sparqlfunctions.constants;

import java.util.ArrayList;

/**
 *
 * used to hold any Formula like windchill_original_formula
 */
public class Formula {
        private ArrayList<Parameter> formulaInputParameters;//ArrayList of input parameters of the formula
        private String  formulaName;//name of the formula
        private String mathematicalExpression;//String Form of the formula present in the ontology
        private Concept computeConcept;//the concpet which is computed by the formula
        private Parameter formulaOutputParameter;//the output parameter of the formula
        private Unit outputUnit;//unit of the output parameter
        
        public Formula() {
             
        }
        
        
        public Formula(String name,Unit unit ) {
             formulaName=name;
             outputUnit=unit;
        }

        public Formula(String name) {
             formulaName=name;
        }

        public Formula(Unit unit,Concept concept) {
             outputUnit=unit;
             computeConcept=concept;
        }
        public Formula(ArrayList<Parameter> inputParameters ,String name,String expression,Concept concept,Parameter outputParameter,Unit unit) {
            
            formulaInputParameters=inputParameters;
            formulaName=name;
            mathematicalExpression=expression;
            computeConcept=concept;
            formulaOutputParameter=outputParameter;
            outputUnit=unit;
            
        }

        public ArrayList<Parameter> getformulaInputParameters() {
                    return formulaInputParameters;
        }

        public String getformulaName() {
                    return formulaName;
        }

        public String getmathematicalExpressions() {
                    return mathematicalExpression;
        }
        
        public Concept getcomputeConcept() {
                    return computeConcept;
        }
        
        public Unit getoutputUnit() {
                    return outputUnit;
        }

        public Parameter getformulaOutputParameter() {
                    return formulaOutputParameter;
        }
        
        public void setformulaInputParameters(ArrayList<Parameter> inputParameters) {
                     formulaInputParameters=inputParameters;
        }

        public void setformulaName(String name) {
                    formulaName = name;
        }

        public void setmathematicalExpressions(String exp) {
                    mathematicalExpression=exp;
        }
        
        public void setcomputeConcept(Concept concept) {
                   computeConcept=concept;
        }

        public void setformulaOutputParameter(Parameter outputParameter) {
                   formulaOutputParameter=outputParameter;
        }
        
        public void setoutputUnit(Unit unit) {
                   outputUnit=unit;
        }
        
        public void printFormulaContents(){
            for(int i=0;i<formulaInputParameters.size();i++) {
                formulaInputParameters.get(i).printParameterContents();
            }
            System.out.println(formulaName);
            System.out.println(mathematicalExpression);
            System.out.println(computeConcept.getConceptName());
            formulaOutputParameter.printParameterContents();
            System.out.println(outputUnit);
            
        }
}
