/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.constants;

import fr.inria.arles.choreos.iots.common.Location;
import java.util.ArrayList;

/**
 *
 * @author abc
 */
public class DataFusionGeneratorImpl {


      public double generateFinalDataValues(ArrayList<Measurement>myMeasurements, Concept mainConcept, Location mainLocation,String fusionFunction){
          int i;
          double valueHolder = 0;
          for(i=0;i<myMeasurements.size();i++) {
              valueHolder=valueHolder+myMeasurements.get(i).getMeasurementValue();
          }
          return valueHolder/i;
      }

}
