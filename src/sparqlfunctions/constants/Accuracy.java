/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sparqlfunctions.constants;

/**
 *
 * used to hold accuracy of the sensors
 */
public class Accuracy {
    private LowerBound lowerBound;//lowest value of the accuracy of a Sensor
    private UpperBound upperBound;//highest value of the accuracy of a Sensor

    
    public Accuracy(LowerBound lowerValueObject,UpperBound upperValueObject) {
        lowerBound=lowerValueObject;
        upperBound=upperValueObject;
    }

    public Accuracy(LowerBound lowerValueObject) {
        lowerBound=lowerValueObject;
        upperBound=new UpperBound(100000);
    }

    public Accuracy(UpperBound upperValueObject) {
        upperBound=upperValueObject;
        lowerBound=new LowerBound(-100000);
    }

    public Accuracy() {
        upperBound=new UpperBound(100000);
        lowerBound=new LowerBound(-100000);
    }

    public LowerBound getLowerValueObject() {
        return lowerBound;
    }

    public UpperBound getUpperValueObject() {
        return upperBound;
    }

    public void setLowerValueObject(LowerBound lowerValueObject) {
         lowerBound=lowerValueObject;
    }

     public void setUpperValueObject(UpperBound upperValueObject) {
         upperBound=upperValueObject;
    }

     public void print() {
         System.out.println("[ "+lowerBound.getLowerBoundValue()+" , "+upperBound.getUpperBoundValue()+" ]");
     }



}
