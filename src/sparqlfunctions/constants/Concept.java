/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sparqlfunctions.constants;

import java.util.ArrayList;

/**
 *
 * used to hold any concept like temperature
 */
public class Concept {
    
        private ArrayList<Formula> providedBy;//the ArrayLIst of Formulas which compute the concept
        private String conceptName;//name of the concept
        public Concept() {
             
        }
        
        
        public Concept(String name) {
             conceptName=name;
        }
        
        
        public Concept(ArrayList<Formula> functions ,String type,Unit unit) {
            providedBy=functions;

        }

        public ArrayList<Formula> getConceptProvidedBy() {
                    return providedBy;
        }

        
        public String getConceptName() {
                    return conceptName;
        }

        public void setConceptProvidedBy(ArrayList<Formula> functions) {
                     providedBy= functions;
        }
        
        public void setconceptName(String name) {
                    conceptName=name;
        }


        


}
