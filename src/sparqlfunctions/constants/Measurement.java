/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.constants;

import fr.inria.arles.choreos.iots.common.Location;

/**
 *
 * @author abc
 */
public class Measurement {
     private Double measurementValue;//value of the concept
     private Location measurementLocation;//unit of the concept that is being compute

     public Measurement(Double value,Location location) {
             measurementValue=value;
             measurementLocation=location;
     }

     public Measurement(Double value) {
             measurementValue=value;
     }

     public double getMeasurementValue() {
             return measurementValue;
     }

     public Location getMeasurementLocation() {
             return measurementLocation;
     }

     public void setMeasurementValue(double value) {
             measurementValue=value;
     }

     public void setMeasurementLocation(Location location) {
             measurementLocation=location;
     }


}
