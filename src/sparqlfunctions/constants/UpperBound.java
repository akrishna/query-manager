/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.constants;

/**
 *
 * @author krishna
 */
public class UpperBound {
    private double upperBoundValue;

    public UpperBound(double upperValue) {

        upperBoundValue=upperValue;
    }

    public double getUpperBoundValue() {
        return upperBoundValue;
    }

    public void setUpperBoundValue(double upperValue) {
        upperBoundValue=upperValue;
    }
}
