/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package sparqlfunctions.constants;

/**
 *
 * @author krishna
 */
public class LowerBound {
    private double lowerBoundValue;

    public LowerBound(double lowerValue) {

        lowerBoundValue=lowerValue;
    }

    public double getLowerBoundValue() {
        return lowerBoundValue;
    }

    public void setLowerBoundValue(double lowerValue) {
        lowerBoundValue=lowerValue;
    }
}
