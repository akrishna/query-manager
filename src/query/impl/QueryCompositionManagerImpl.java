package query.impl;
import query.constants.Where;
import query.constants.Setter;
import query.constants.Selector;


import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;

import fr.inria.arles.choreos.iots.common.Location;

import java.util.*;
import query.interfaces.QueryCompositionManager;

import sparqlfunctions.constants.Accuracy;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Formula;
import sparqlfunctions.constants.Measurement;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.Range;
import sparqlfunctions.impl.SparqlImpl;
import sparqlfunctions.constants.Unit;


public class QueryCompositionManagerImpl implements QueryCompositionManager {

        SparqlImpl sparql;//used to access Sparql methods
        //not used till now
	private String queryExpression;
        //gives exact structure of the SubQuery
        public HashMap<SubQueryGeneratorImpl,Formula> subQueryHashMap=new HashMap();
        //flat data structure to used for checking new Concept
        public static HashMap<Concept,ArrayList<SubQueryGeneratorImpl>> subQueryDecomposed=new HashMap();
	private Location locationToMeasure;
        //this will store all the Concept to be computed from Query object
	private ArrayList<Concept> physicalConcepts;
        private Selector select;
        private Setter set=new Setter();
        
	
        public QueryCompositionManagerImpl(String queryExp) {
		queryExpression = queryExp;
	}
         /**
	 * constructor of the Query
	 * @param querySelector: Selector object of the Query 
         * @param locationOfInterest: Location object  
	 */
        public QueryCompositionManagerImpl(Selector querySelector,
			 Location locationOfInterest) {
            select=querySelector;
            locationToMeasure = locationOfInterest;
	    physicalConcepts = select.getConceptListFromSelector();
            createSubQuery();
        }
        
            /**
	 * constructor of the Query
	 * @param querySelector: Selector object of the Query 
         * @param querySetter: Setter object of the Query 
         * @param locationOfInterest: Location object  
	 */
        public QueryCompositionManagerImpl(Selector querySelector,
                         Setter querySetter,
			 Location locationOfInterest) {
            select=querySelector;
            locationToMeasure = locationOfInterest;
	    physicalConcepts = select.getConceptListFromSelector();
            set=querySetter;
            createSubQuery();//this will start recursion
        }
        
         /**
	 * constructor of the Query
	 * @param conceptsToMeasure: ArrayList of concepts to be measured 
         * @param locationOfInterest: Location object  
	 */
        public QueryCompositionManagerImpl(ArrayList<Concept> conceptsToMeasure,
			 Location locationOfInterest) {
            locationToMeasure = locationOfInterest;
	    physicalConcepts = conceptsToMeasure;
            createSubQuery();
        }
        
              /**
	 * called by the constructor :used for creating the subqueries of the main query
         * create both the flat(used for skipping checked concept) and graph data structure(exact structure of subQuery)
         */
        private void createSubQuery() {
                sparql=new SparqlImpl();
                SubQueryGeneratorImpl subQuery;//to hold the SubQuery object
                ArrayList<Parameter> subQueryParameter;
                ArrayList<Formula> subQueryFormula;
                main:for(int i=0;i<physicalConcepts.size();i++)
                  try { 
                      ArrayList<SubQueryGeneratorImpl> subQueries = new ArrayList();//to put in flat datastructure
                      //to know if the concept has been added in flat data structure
                      for(Iterator<Concept> it =subQueryDecomposed.keySet().iterator();it.hasNext();) {
                            Concept key = it.next();
                            if(physicalConcepts.get(i).getConceptName().equals(key.getConceptName())) {
                                if(subQueryDecomposed.get(key).isEmpty()) {
                                    System.err.println("no formula exist for this concept in ontology to expend this");
                                    subQueryDecomposed.put(physicalConcepts.get(i),new ArrayList<SubQueryGeneratorImpl>());
                                }
                                for(int k=0;k<subQueryDecomposed.get(key).size();k++) {
                                    subQueryHashMap.put(subQueryDecomposed.get(key).get(k),
                                        subQueryDecomposed.get(key).get(k).getSubQueryFormula());
                            
                                } continue main;//go for the next concept
                            }   
                      }     
                      //if Concept has not been added to flat data structure
                      subQueryFormula=sparql.getFormulaList(physicalConcepts.get(i));
                      for(int j=0;j<subQueryFormula.size();j++) {
                            subQueryParameter=sparql.getInputParameters(
                                subQueryFormula.get(j)
                                .getformulaName());
                            subQuery=new SubQueryGeneratorImpl(
                                new Selector(subQueryParameter),locationToMeasure,i,subQueryFormula.get(j),this);
                            subQueryHashMap.put(subQuery
                                ,subQueryFormula.get(j));
                            subQueries.add(subQuery);
                        
                      }
                      subQueryDecomposed.put(physicalConcepts.get(i),subQueries);
                  } catch (NullPointerException e) {
                      System.err.println("no formula exist for this concept in ontology to expend this");
                      subQueryDecomposed.put(physicalConcepts.get(i),new ArrayList<SubQueryGeneratorImpl>());
   
                    }
                
            
        }
        
                /**
	 * fetch out all the sub Concepts of the Query to do search for available data
         * this is returning all the concepts stored in the flat data structure
	 * @return the ArrayList of concepts(both sub Concept and main Concept)
	 */
                
    @Override
        public ArrayList<Concept> expandConcept() {
            ArrayList<Concept> expendedConcept=new ArrayList();
            expendedConcept.addAll(subQueryDecomposed.keySet());
            return expendedConcept;
            
        }
         
         /**
	 * TO COMPUTE CONCEPT,it internally call computeConcept FUNCTION IN Sparql CLASS
	 * @param parameterValues: parameter value HashMap
         * @return: return a HashMap having concept to be evaluated as key and value of concept as value of the HashMap 
         * put the value of concept as null when we don't have enough parameters to compute the Concept
         * @throws: UnknownFunctionException ,UnparsableExpressionException
            */
        
    @Override
         public HashMap<Concept,FinalResult> computeQueryConcept(HashMap<Parameter,Measurement> parameterValues,
                String fusionFunction)
                throws UnknownFunctionException, UnparsableExpressionException {
            ArrayList<Parameter> foundParameters;
            FinalResult valueHolder;
            HashMap<Formula,Measurement> formulaValues;
            HashMap<Concept,FinalResult> computeResult =new HashMap();
            main:for(int i=0;i<physicalConcepts.size();i++) {
                for(Iterator<Concept> it = subQueryDecomposed.keySet().iterator();it.hasNext();) {
                            Concept conceptkey = it.next();
                            if(physicalConcepts.get(i).getConceptName().equals(
                                    conceptkey.getConceptName())) {
                                foundParameters=sparql.getParameterValue(
                                    physicalConcepts.get(i),parameterValues);
                                if(foundParameters==null) {//no parameter is their linked with the same concept
                                    //if expansion is not possible
                                    if(subQueryDecomposed.get(conceptkey).isEmpty()) {
                                         //since no similar parameter found at first step
                                         //also no expension is their so the given Concept cant be evaluated
                                         System.err.println("cant be computed from ontology");
                                         computeResult.put(physicalConcepts.get(i),new FinalResult(null));//put tha value as null
                                         continue main; 
                                    }
                                    //use expansion to compute concept
                                    else {
                                         formulaValues=sparql.computeTillExpend(
                                         subQueryDecomposed.get(conceptkey),
                                                 parameterValues,
                                                 fusionFunction);
                                         //let us check below that if the conditions(like for required units)
                                         //has been set by setter or not
                                         if(set.isset()==true && set.getConstraint().getUnitHashMap().containsKey(physicalConcepts.get(i).getConceptName())) {
                                            //handle query to be failed seprately as a special case
                                            if(set.getConstraint().getBadQuery()==true) {
                                                System.out.println("THE QUERY FAILS");
                                                return null;
                                            }
                                            valueHolder=sparql.finalConversion(formulaValues,
                                            set.getConstraint().getUnitHashMap().get(physicalConcepts.get(i).getConceptName()),
                                                 fusionFunction,
                                                 physicalConcepts.get(i),
                                                 locationToMeasure

                                                 );//ArrayList of unit set by Setter
                                        }
                                        else {
                                             //handle query to be failed seprately as a special case
                                            if( set.isset()==true && set.getConstraint().getBadQuery()==true) {
                                                System.out.println("THE QUERY FAILS");
                                                return null;
                                            }
                                            valueHolder=sparql.finalConversion(formulaValues,
                                                new ArrayList<Unit>(),
                                                fusionFunction,
                                                physicalConcepts.get(i),
                                                locationToMeasure
                                                );
                                        }
                                         //computeResult is the HashMap where all the Concept result
                                         //are stored in the required unit set by Setter
                                         computeResult.put(physicalConcepts.get(i),valueHolder);
                                         continue main;
                                    }
                                }
                                    //here foundParameter must be present so here we will add
                                    //a virtual formula in the incoming Formula HashMap
                                    //which stands for computing the concept when any Parameter having
                                    //the same concept is already their in the Parameter value hashmap.
                                   
                                    //computeTillExpend() is a nostop version of computeConcept()
                                    //here all possible path for computing the concept are
                                    //taken in account(even starting from itself)
                                    formulaValues=sparql.computeTillExpend(
                                            subQueryDecomposed.get(conceptkey),parameterValues,fusionFunction);
                                    formulaValues.putAll(sparql.virtualFormula(
                                            foundParameters));//it will return the value in the unit of 1st available unit)
                                    if(set.isset()==true && set.getConstraint().getUnitHashMap().containsKey(physicalConcepts.get(i).getConceptName())) {
                                        //handle query to be failed seprately as a special case
                                        if(set.getConstraint().getBadQuery()==true) {
                                                System.out.println("THE QUERY FAILS");
                                                return null;
                                        }
                                        valueHolder=sparql.finalConversion(formulaValues,
                                                set.getConstraint().getUnitHashMap().get(physicalConcepts.get(i).getConceptName()),
                                                fusionFunction,
                                                physicalConcepts.get(i),
                                                locationToMeasure
                                                 );//unit set by Setter is
                                                                 //  used in argument
                                    }
                                    else {
                                        //handle query to be failed seprately as a special case
                                        if(set.isset()==true && set.getConstraint().getBadQuery()==true) {
                                                System.out.println("THE QUERY FAILS");
                                                return null;
                                        }
                                        valueHolder=sparql.finalConversion(formulaValues,
                                                new ArrayList<Unit>(),
                                                fusionFunction,
                                                physicalConcepts.get(i),
                                                locationToMeasure
                                                );
                                    }
                                    computeResult.put(physicalConcepts.get(i),valueHolder);
                                    continue main;
                                }

                           }
             }
                
                return computeResult;
        }

              /**
	 * used to find all set of possible desired unit in which final value of concept is allowed
         * @param concept: Concept object for which the set of desired unit is required
	 * @return ArrayList of allowed units for the concept
	 */

        public ArrayList<Unit> getRequiredUnits(Concept concept) {
            return set.getConstraint().getUnitHashMap().get(concept.getConceptName());
        }
               /**
	 * used to reset all constraints including units,Accuracy,Range etc
         * @param condition: set of all conditions bind by AND/OR or &/|
	 */

    @Override
        public void resetRequiredConstraint(String condition) {
            set.setConstraint(new Where(condition));
        }
                /**
	 * used to add a specific unit as the allowed unit for the concept
         * @param concept: Concept object for which we need to set the unit as the allowed unit
	 * @param unit: the unit that we want to add as allowable unit for the concept
	 */

        public void addRequiredUnits(Concept concept,Unit unit) {
            ArrayList<Unit> unitList=set.getConstraint().getUnitHashMap().get(concept.getConceptName());
            unitList.add(unit);
            set.getConstraint().setUnitHashMap(concept.getConceptName(),unitList);
        }

       /**
	 * used to find all set of possible desired Accuracy in which final value of concept is allowed
         * @param concept: Concept object for which the set of desired Accuracy is required
	 * @return ArrayList of allowed Accuracy for the concept
	 */

        public ArrayList<Accuracy> getRequiredAccuracy(Concept concept) {
            return set.getConstraint().getAccuracyHashMap().get(concept.getConceptName());
        }

                /**
	 * used to add a specific Accuracy as the allowed Accuracy for the concept
         * @param concept: Concept object for which we need to set the accuracy
	 * @param unit: the Accuracy that we want to add
	 */

        public void addRequiredAccuracy(Concept concept,Accuracy accuracy) {
            ArrayList<Accuracy> accuracyList=set.getConstraint().getAccuracyHashMap().get(concept.getConceptName());
            accuracyList.add(accuracy);
            set.getConstraint().setAccuracyHashMap(concept.getConceptName(),accuracyList);
        }

              /**
	 * used to find all set of possible desired Range
         * @param concept: Concept object for which the set of desired Range is required
	 * @return ArrayList of allowed Range for the concept
	 */

        public ArrayList<Range> getRequiredRange(Concept concept) {
            return set.getConstraint().getRangeHashMap().get(concept.getConceptName());
        }
                /**
	 * used to add a specific Range
         * @param concept: Concept object for which we need to set the Range
	 * @param unit: the Range that we want to add
	 */

        public void addRequiredRange(Concept concept,Range accuracy) {
            ArrayList<Range> rangeList=set.getConstraint().getRangeHashMap().get(concept.getConceptName());
            rangeList.add(accuracy);
            set.getConstraint().setRangeHashMap(concept.getConceptName(),rangeList);
        }


	public String getQueryExpression() {
		return queryExpression;
	}

	public double getCoverageRequirement() {
		return 0;
	}

	public Location getLocationOfInterest() {
		return locationToMeasure;
	}

        public void setQueryExpression(String exp) {
		queryExpression = exp;
	}
        
        public void setSelector(Selector S) {
                select=S;
        }
        
        public Selector getSelector() {
                return select;
        }
        
        public ArrayList<Concept> getConceptsToMeasure() {
            return (select.getConceptListFromSelector());
        }
        
        public HashMap<SubQueryGeneratorImpl,Formula> getSubQueryHashMap() {
            return subQueryHashMap;
        }
        
        public HashMap<Concept,ArrayList<SubQueryGeneratorImpl>> getFlatDataStructure() {
            return subQueryDecomposed;
        }

        public Setter getSetter(){
             return set;
        }


        


}
