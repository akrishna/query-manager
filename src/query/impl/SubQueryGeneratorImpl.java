package query.impl;
import query.constants.Setter;
import fr.inria.arles.choreos.iots.common.Location;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Formula;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.impl.SparqlImpl;
import query.constants.Selector;
import query.interfaces.SubQueryGenerator;

public class SubQueryGeneratorImpl implements SubQueryGenerator{
	private String subQueryExpression;
        private  Formula subQueryFormula;//formula associated with the Sub Query
        private QueryCompositionManagerImpl mainQuery;//to link with main Query object(of which it is SubQuery)
        private int subQueryNumber;//to know which SubQuery is due to which Parameter
        public boolean furtherSubQuery=false;//to ensure wheather a SubQuery have a further SubSubQuery:condition of recursion termination
        private HashMap<SubQueryGeneratorImpl,Formula> subQueryHashMap=new HashMap();
	private Location locationToMeasure;
        private Selector select;
        private Setter set;
        SparqlImpl sparql;
        
	public SubQueryGeneratorImpl(String queryExp) {
		subQueryExpression = queryExp;
	}
             /**
	 * constructor of the SubQuery
	 * @param querySelector: Selector object of the SubQuery 
         * @param locationOfInterest: Location object  
         * @param number: to know which SubQuery is for which Parameter 
         * @param formula: Formula object of the SubQuery  
         * @param query: main Query of the SubQuery 
	 */
        public SubQueryGeneratorImpl(Selector subQuerySelector,
			 Location locationOfInterest,int number,Formula formula,QueryCompositionManagerImpl query) {
            locationToMeasure = locationOfInterest;
	    select=subQuerySelector;
            subQueryNumber=number;
            subQueryFormula=formula;
            mainQuery=query;
            createSubQuery();
        }
                 /**
	 * called by the constructor of the SubQuery
         * start the recursion for creating expansion of Concept
	 */
        private void createSubQuery() { 
            sparql=new SparqlImpl();
            SubQueryGeneratorImpl subQueryHolderObject;//to hold the SubQuery object
            ArrayList<Concept> concept=getConceptsOfParametersToMeasure();
            ArrayList<Parameter> subQueryParameter;
            ArrayList<Formula> subQueryFormulaHolder;
            main:for(int i=0;i<concept.size();i++) {
                ArrayList<SubQueryGeneratorImpl> subQueryList = new ArrayList();
                        if(sparql.getFormulaList(concept.get(i))==null) {//if there is no formula calculating the concept
                            QueryCompositionManagerImpl.subQueryDecomposed.put(concept.get(i),new ArrayList<SubQueryGeneratorImpl>());//we add a empty ArrayList
                            continue;
                        }
                        //search if the Concept has already been expended
                        for(Iterator<Concept> it = QueryCompositionManagerImpl.subQueryDecomposed.keySet().iterator();it.hasNext();) {
                            Concept key = it.next();
                            if(concept.get(i).getConceptName().equals(key.getConceptName())) {
                                furtherSubQuery=true;//we are now expanding the Sub Query
                                for(int k=0;k<QueryCompositionManagerImpl.subQueryDecomposed.get(key).size();k++) {
                                    subQueryHashMap.put(
                                            QueryCompositionManagerImpl.subQueryDecomposed.get(key).get(k),
                                        QueryCompositionManagerImpl.subQueryDecomposed.get(key).get(k).subQueryFormula);
                            
                                }
                            
                                continue main;
                           
                            }    
                        }   
                        furtherSubQuery=true;//we are now expanding the Sub Query
                        subQueryFormulaHolder=sparql.getFormulaList(concept.get(i));
                        for(int j=0;j<subQueryFormulaHolder.size();j++) {
                                subQueryParameter=sparql.getInputParameters(
                                subQueryFormulaHolder.get(j)
                                    .getformulaName());
                                subQueryHolderObject=new SubQueryGeneratorImpl(
                                new Selector(subQueryParameter),locationToMeasure,i,subQueryFormulaHolder.get(j),mainQuery);
                                subQueryHashMap.put(subQueryHolderObject
                                ,subQueryFormulaHolder.get(j));
                                subQueryList.add(subQueryHolderObject);
                        }
                        QueryCompositionManagerImpl.subQueryDecomposed.put(concept.get(i),subQueryList);//put in flat data structure
                        
                }
                  
            

            
        }
                 /**
	 * to get Concept to be computed by the SubQuery
         * @return :  ArrayList of Concepts
	  */
               
    @Override
       public ArrayList<Concept> getConceptsOfParametersToMeasure() {
            ArrayList<Concept> conceptOfParameters=new ArrayList();
            for(int i=0;i<select.getParameterListFromSelector().size();i++) {
                conceptOfParameters.add(
                        sparql.getPhysicalConcept(
                            select.getParameterListFromSelector()
                        .get(i).getParameterName()));
            }
            return conceptOfParameters;
        }
        
        public HashMap<SubQueryGeneratorImpl,Formula> getSubQueryHashMap() {
            return subQueryHashMap;
        }

        public String getQueryExpression() {
		return subQueryExpression;
	}

	public double getCoverageRequirement() {
		return 0;
	}

	public Location getLocationOfInterest() {
		return locationToMeasure;
	}

	public void setQueryExpression(String exp) {
		subQueryExpression = exp;
	}
        
        public void setSelector(Selector s) {
                select=s;
        }
        public Selector getSelector() {
                return select;
        }
        
        public ArrayList<Concept> getConceptsToMeasure() {
            return (select.getConceptListFromSelector());
        }
        
        public final ArrayList<Parameter> getParametersToMeasure() {
            return (select.getParameterListFromSelector());
        }
        
        public int getSubQueryNumber() {
                return subQueryNumber;
        }
        
    @Override
        public Formula getSubQueryFormula() {
                return subQueryFormula;
        }
        
        public ArrayList<Formula> getFormulasToMeasure() {
            return (select.getFormulaListFromSelector());
        }
        
     
 }
