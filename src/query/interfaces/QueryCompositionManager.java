/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package query.interfaces;

import de.congrace.exp4j.UnknownFunctionException;
import de.congrace.exp4j.UnparsableExpressionException;
import java.util.ArrayList;
import java.util.HashMap;

import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.FinalResult;
import sparqlfunctions.constants.Measurement;
import sparqlfunctions.constants.Parameter;


/**
 *
 * @author krishna
 */
public interface QueryCompositionManager {


         /**
	 * fetch out all the sub Concepts of the Query to do search for available data
         * this is returning all the concepts stored in the flat data structure
	 * @return the ArrayList of concepts(both sub Concept and main Concept)
	 */

         ArrayList<Concept> expandConcept();

         /**
	 * TO COMPUTE CONCEPT,it internally call computeConcept FUNCTION IN Sparql CLASS
	 * @param parameterValues: parameter value HashMap
         * @return: return a HashMap having concept to be evaluated as key and value of concept as value of the HashMap
         * put the value of concept as null when we don't have enough parameters to compute the Concept
         * @throws: UnknownFunctionException ,UnparsableExpressionException
         */

        HashMap<Concept,FinalResult> computeQueryConcept(HashMap<Parameter,Measurement> parameterValues,
                String fusionFunction)
                throws UnknownFunctionException, UnparsableExpressionException;



         /**
	 * used to reset all constraints including units,Accuracy,Range etc
         * @param condition: set of all conditions bind by AND/OR or &/|
	 */

        void resetRequiredConstraint(String condition);


}
