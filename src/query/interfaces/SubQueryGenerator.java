/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package query.interfaces;

import java.util.ArrayList;
import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Formula;



/**
 *
 * @author krishna
 */
public interface SubQueryGenerator {


         /**
	 * to get Concept to be computed by the SubQuery
         * @return :  ArrayList of Concepts
	 */

         ArrayList<Concept> getConceptsOfParametersToMeasure();


          /**
	 * to get Formula associated with the Sub Query
         * @return : Formula object
	 */

        Formula getSubQueryFormula();

}
