/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package query.constants;


/**
 *
 * @author krishna
 */
public class Setter {
        
        private Where constraint;
        private Having having;
        private GroupBy groupBy;
        private OrderBy orderBy;
        private boolean setCondition=false;//it will help to know from a Query structure
                                           //wheather a settor was passed or not

        /*
         * constructor of the Setter class
         * capable of having variable number of arguments
         * store the local variables depending upon the the type of object passed as argumnet
         */
        public<T> Setter(T... selectorList) {
               
            for (int i =0 ; i<selectorList.length ; i++) {
                String componentName=selectorList[i].getClass().toString();
                int j=componentName.lastIndexOf('.');
                //if it is a object of Where class
                if(componentName.substring(j+1).equals("Where")) {
                    setCondition=true;//since here we are sure a settor is being passed
                    constraint=((Where)selectorList[i]);
                }
                //if it is a object of Having class
                else if(componentName.substring(j+1).equals("Having")) {
                    having=((Having)selectorList[i]);
                }
                //if it is a object of OrderBy class
                else if(componentName.substring(j+1).equals("OrderBy")) {
                    orderBy=((OrderBy)selectorList[i]);
                }
                //if it is a object of GroupBy class
                else if(componentName.substring(j+1).equals("GroupBy")){
                    groupBy=((GroupBy)selectorList[i]);
                }
            }
         }
            
            public void setHaving(Having H) {
                    having=H;
            }

            public Having getHaving() {
                    return having;
            }

            public void setGroupBy(GroupBy G) {
                    groupBy=G;
            }

            public GroupBy getGroupBy() {
                    return groupBy;
            }

            public void setOrderBy(OrderBy O) {
                    orderBy=O;
            }

            public OrderBy getOrderBy() {
                    return orderBy;
            }

            public void setConstraint(Where W) {
                    constraint=W;
            }

            public Where getConstraint() {
                    return constraint;
            }

            public boolean isset() {
                return setCondition;
            }
        
 }

