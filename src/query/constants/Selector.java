/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package query.constants;

import java.util.ArrayList;

import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.Formula;


/**
 *
 * @author krishna
 */
public class Selector {
    
    public ArrayList<String> selectComponentName=new ArrayList();
    private ArrayList<Concept> selectConcept=new ArrayList();
    public ArrayList<Parameter> selectParameter=new ArrayList();
    private ArrayList<Formula> selectFormula=new ArrayList();
    
    public<T> Selector(T... selectorList) {
        for (int i =0 ; i<selectorList.length ; i++) {
            
            String componentName=selectorList[i].getClass().toString();
            //System.out.println(componentName);
            int j=componentName.lastIndexOf('.');
            // System.out.println(componentName.substring(j+1)+" "+selectorList[i]+" "+((Concept)selectorList[i]).getConceptName());
            if(componentName.substring(j+1).equals("Formula")) {
                selectComponentName.add(((Formula)selectorList[i]).getformulaName());
                selectFormula.add((Formula)selectorList[i]);
            }
            else if(componentName.substring(j+1).equals("Parameter")) {
                selectComponentName.add(((Parameter)selectorList[i]).getParameterName());
                selectParameter.add((Parameter)selectorList[i]);
            }
            else if(componentName.substring(j+1).equals("Concept")) {
                selectComponentName.add(((Concept)selectorList[i]).getConceptName());
                selectConcept.add((Concept)selectorList[i]);
            }
            else {
                selectComponentName.add(selectorList[i].toString());
            }
        }
    }
    public<T> Selector(ArrayList<T> component) {
            String componentName=component.get(0).getClass().toString();
           // System.out.println(componentName);
            int j=componentName.lastIndexOf('.');
            // System.out.println(componentName.substring(j+1)+" "+selectorList[i]+" "+((Concept)selectorList[i]).getConceptName());
            if(componentName.substring(j+1).equals("Formula")) {
                selectFormula=(ArrayList<Formula>) component;
            }
            else if(componentName.substring(j+1).equals("Parameter")) {
                selectParameter=(ArrayList<Parameter>) component;
            }
            else if(componentName.substring(j+1).equals("Concept")) {
                selectConcept=(ArrayList<Concept>) component;
            }
           
    }
   
  
    public void printComponentList() {
        for (int i =0; i<selectComponentName.size(); i++) {
           System.out.println(selectComponentName.get(i));
        }
        
    }
    
    public ArrayList<Concept> getConceptListFromSelector() {
        return selectConcept;
    }
    
    public ArrayList<Parameter> getParameterListFromSelector() {
        return selectParameter;
    }
    
    public ArrayList<Formula> getFormulaListFromSelector() {
        return selectFormula;
    }
    
    
    
}
