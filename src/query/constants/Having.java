/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package query.constants;

import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Formula;
import sparqlfunctions.constants.Parameter;

/**
 *
 * @author krishna
 */
public class Having {
      private String HavingExpression;
      public<T> Having(T componentName,String operation,int componentValue) {
                String className=componentName.getClass().toString();
                //  System.out.println(componentName);
                int j=className.lastIndexOf('.');
                // System.out.println(componentName.substring(j+1)+" "+selectorList[i]+" "+((Concept)selectorList[i]).getConceptName());
                if(className.substring(j+1).equals("Formula")) {
                HavingExpression=((Formula)componentName).getformulaName()+ operation +componentValue;
                }
                else if(className.substring(j+1).equals("Parameter")) {
                HavingExpression=((Parameter)componentName).getParameterName()+ operation +componentValue;
                }
                else if(className.substring(j+1).equals("Concept")) {
                HavingExpression=((Concept)componentName).getConceptName()+ operation +componentValue;
                }
                else {
                HavingExpression=componentName.toString()+operation+componentValue;
                }
    }
      public void printHavingExpression() {
       
           System.out.println(HavingExpression);
        
    }
}
