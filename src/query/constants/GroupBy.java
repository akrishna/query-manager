/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package query.constants;



import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.Formula;


/**
 *
 * @author krishna
 */
public class GroupBy {
    
    private String groupByComponentName;
    public<T> GroupBy(T groupByValue) {
        
            String componentName=groupByValue.getClass().toString();
            //  System.out.println(componentName);
            int j=componentName.lastIndexOf('.');
            // System.out.println(componentName.substring(j+1)+" "+selectorList[i]+" "+((Concept)selectorList[i]).getConceptName());
            if(componentName.substring(j+1).equals("Formula")) {
                groupByComponentName=((Formula)groupByValue).getformulaName();
            }
            else if(componentName.substring(j+1).equals("Parameter")) {
                groupByComponentName=((Parameter)groupByValue).getParameterName();
            }
            else if(componentName.substring(j+1).equals("Concept")) {
                groupByComponentName=((Concept)groupByValue).getConceptName();
            }
            else {
                groupByComponentName=groupByValue.toString();
            }
     }
    
   
  
    public void printComponentName() {
       
           System.out.println(groupByComponentName);
        
    }
    
    
    
}
