/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package query.constants;

import sparqlfunctions.constants.Accuracy;
import sparqlfunctions.constants.IntervalOperations;
import sparqlfunctions.constants.LowerBound;
import sparqlfunctions.constants.Range;
import sparqlfunctions.constants.Unit;
import sparqlfunctions.constants.UpperBound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;



/**
 *
 * @author krishna
 */
public class Where {
    
    private HashMap<String,ArrayList<Unit>>  unitHashMap=new HashMap();//store the required units of
                                                                       //Concepts <ConceptName,Unit>
    private static final String operators = "&|";//operator list
    private HashMap<String,ArrayList<Accuracy>>  accuracyHashMap=new HashMap();//store the accuracy requirement for a concept
    private HashMap<String,ArrayList<Range>>  rangeHashMap=new HashMap();
    private static final String specialChars = ".=_><-";//special characters allowed in a constraint name
    private boolean badQuery=false;//it will help to know to declare a Query a bad Query and make it fail
          /**
         * constructor for the class
         * calls other functions depending upon the number of conditions
	 * @param whereExp: a boolean expression string which will be parsed to get all required conditions
	 */

    public Where(String whereExp) {
            
               whereExp=whereExp.replace(" ","").replace("AND","&").replace("OR","|");
               whereExp=convert2Postfix(whereExp);
               if(!whereExp.contains("&")&&!whereExp.contains("|")) {//only a single condition since no &,|
                   setCondition(whereExp);
               }
               else {
                   parsePostfix(whereExp);//call a function in presence of &,|

               }

             

    }
     /**
         * it gets call from setCondition
         * it parse a string like concept1.unit.equals=degreeC to return unit as the attribute type
	 * @param condition: a string which need to be parsed
	 */

    private String attributeType(String condition) {
               int pos1,pos2;
               pos1=n_indexof(condition,'.',1);//first occourance of .
               pos2=n_indexof(condition,'.',2);//second occourance of .
               return condition.substring(pos1+1,pos2);
    }

        /**
         * it gets call from setCondition
         * it parse a string like concept1.unit.equals=degreeC to return concept1 as the concept name
	 * @param condition: a string which need to be parsed
	 */

    private String conceptName(String condition) {
               int pos1;
               pos1=n_indexof(condition,'.',1);//first occourance of .
               return condition.substring(0,pos1);
    }

    /**
         * it gets call from constructor for checking the syntax of boolean expression
         * it parse a string like concept1.unit.equals=degreeC to return true if the string is of correct syntax
         * it check whether a condition string have two dot and one relational operator or not
	 * @param condition: a string which need to be parsed
	 */
    
    private boolean syntaxPreCheck(String condition) {
               int pos1;
               pos1=n_indexof(condition,'.',2);//second occourance of . since there must be two dot
               return pos1>=0 && (condition.indexOf('=')>=0 | condition.indexOf('<')>=0 | condition.indexOf('>')>=0);
    }

       /**
         * it gets call from setCondition
         * it parse a string like concept1.unit.equals=degreeC to return degreeC as the attribute(unit) value
	 * @param condition: a string which need to be parsed
	 */

    private String attributeValue(String condition) {
               int brac1;
               //we need to make cases for relational operators
               if(!(condition.indexOf("<=") < 0)) {
                  brac1=condition.indexOf('=');
               }
               else if(!(condition.indexOf(">=") < 0)) {
                  brac1=condition.indexOf('=');
               }
               else if(!(condition.indexOf('>') < 0)){
                  brac1=condition.indexOf('>');
               }
               else if(!(condition.indexOf('<') < 0)) {
                  brac1=condition.indexOf('<');
               }
               else {
                  brac1=condition.indexOf('=');
               }
               return condition.substring(brac1+1);
    }

           /**
         * it gets call from setCondition
         * it parse a string like concept1.unit.equals=degreeC to return type of relation used like =,<,>,<=,>=
	 * @param condition: a string which need to be parsed
	 */

    private String relationType(String condition) {
               
               String relationType;

               if(!(condition.indexOf(">=") < 0)) {
                   relationType="greaterThanOrEqualTo";
               }
               else if(!(condition.indexOf("<=") < 0)) {
                   relationType="lesserThanOrEqualTo";
               }
               else if(!(condition.indexOf('>') < 0)) {
                   relationType="greaterThan";
               }
               else if(!(condition.indexOf('<') < 0)) {
                   relationType="lesserThan";
               }
               else {
                   relationType="equal";
               }

               return relationType;
    }

    /**
         * it gets call from constructor if there is only one condition
         * or form the method parsePostfix() by parsing the postfix form of the given boolean expression string
         * it parse a string like concept.unit.equals=degreeC to store degreeC as the allowable unit for the concept1
	 * @param condition: a string which need to be parsed
	 */
     
    private void setCondition(String condition) {

               String attributeType,conceptName,attributeValue,relationType;
               attributeType=attributeType(condition);
               attributeValue=attributeValue(condition);
               conceptName=conceptName(condition);
               relationType=relationType(condition);
               Accuracy accuracy;
               Range range;
               // take the various possible cases of attributes
               // what type of constraint we can impose like unit,accuracy,range etc
               if(attributeType.equals("unit")) {//if attribute type is unit
                   if(!unitHashMap.containsKey(conceptName)) {//check if there is already a key in hashmap
                        ArrayList<Unit> unitList=new ArrayList();
                        unitList.add(new Unit(attributeValue));
                        unitHashMap.put(conceptName,unitList);
                   }
                   else {//update the hashmap value mapped by the present key(concept name)
                        unitHashMap.get(conceptName).add(new Unit(attributeValue));
                   }
               }
              else if(attributeType.equals("accuracy")) {//if attribute type is accuracy
                    // we need to create the Accuracy object depending upon the relation used in the condition
                    // i.e >,<,>=,<=,= etc
                   if(relationType.equals("lesserThan")) {
                        //upperBound is given
                        // subtract 1 since it is given strictly lesser than
                        accuracy=new Accuracy(new UpperBound(Double.parseDouble(attributeValue)-1));
                   }
                   else if(relationType.equals("greaterThan")) {
                        //lowerBound is given
                        // add 1 since it is given strictly greater than
                        accuracy=new Accuracy(new LowerBound(Double.parseDouble(attributeValue)+1));
                   }
                   else if(relationType.equals("greaterThanOrEqualTo")) {
                        accuracy=new Accuracy(new LowerBound(Double.parseDouble(attributeValue)));
                   }
                   else if(relationType.equals("lesserThanOrEqualTo")) {
                        accuracy=new Accuracy(new UpperBound(Double.parseDouble(attributeValue)));
                   }
                   else {
                        accuracy=new Accuracy(new LowerBound(Double.parseDouble(attributeValue)),new UpperBound(Double.parseDouble(attributeValue)));
                   }

                   if(!accuracyHashMap.containsKey(conceptName)) {//if the HashMap doesn't already contains key
                        ArrayList<Accuracy> accuracyList=new ArrayList();
                        accuracyList.add(accuracy);
                        accuracyHashMap.put(conceptName,accuracyList);
                   }
                   else {//update the HashMap if it contains the key already
                        accuracyHashMap.get(conceptName).add(accuracy);
                   }
              }

                else if(attributeType.equals("range")) {//if attribute type is range
                    if(relationType.equals("lesserThan")) {
                        //upperBound is given
                        // subtract 1 since it is given strictly lesser than
                        range=new Range(new UpperBound(Double.parseDouble(attributeValue)-1));
                    }
                    else if(relationType.equals("greaterThan")) {
                        //lowerBound is given
                        // add 1 since it is given strictly greater than
                        range=new Range(new LowerBound(Double.parseDouble(attributeValue)+1));
                    }
                    else if(relationType.equals("greaterThanOrEqualTo")) {
                        range=new Range(new LowerBound(Double.parseDouble(attributeValue)));
                    }
                    else if(relationType.equals("lesserThanOrEqualTo")) {
                        range=new Range(new UpperBound(Double.parseDouble(attributeValue)));
                    }
                    else {
                        range=new Range(new LowerBound(Double.parseDouble(attributeValue)),new UpperBound(Double.parseDouble(attributeValue)));
                    }

                    if(!rangeHashMap.containsKey(conceptName)) {//if the HashMap doesn't already contains key
                        ArrayList<Range> rangeList=new ArrayList();
                        rangeList.add(range);
                        rangeHashMap.put(conceptName,rangeList);
                   }
                   else {//update the HashMap if it contains the key already
                        rangeHashMap.get(conceptName).add(range);
                   }
              }

    }

     /**
         * it gets call from constructor when there is more than one condition
         * it parse the postfix form of the given boolean expression containing various constraints
	 * @param booleanString: a string which is the postfix form
	 */


    private void parsePostfix(String booleanString) {
               boolean samePropertyWithAnd=false;
               ArrayList<Unit> unitList;
               ArrayList<Accuracy> accuracyList;
               ArrayList<Range> rangeList;
               Accuracy netAccuracy;
               Range netRange;
               int operatorNumber=0;//it will keep the number of operator(&/|) in the boolean expression
               Stack<String> attributeValueStack=new Stack();//used to store attribute value like degreeC
               Stack<String> attributeTypeStack=new Stack();//used to store unit,accuracy etc
               Stack<String> conceptNameStack=new Stack();//used to store name of the concept
               StringBuilder condition = new StringBuilder();//a individual condition out of full boolean expression
               String firstCondition,secondCondition,firstConcept,secondConcept,firstAttributeValue,secondAttributeValue;
               for(int i=0;i<booleanString.length();i++) {
                   //when we encounter a alphanumberic character we will iterate till
                   //when we find the full condition string
                   if(isAlphaNum(booleanString.charAt(i))) {
                       condition.delete(0, condition.length());
                       //iterate till when we find a operator or a ' ' denoting the end of condition
                       while(!isOperator(booleanString.charAt(i)) && !(booleanString.charAt(i)==' ')) {
                           condition.append(booleanString.charAt(i));
                           i++;
                       }
                       //check wheather condition is of standard format or not
                        if(syntaxPreCheck(condition.toString())==false) {
                           //if the condition is not of the standard format we declare the Query as bad
                           System.err.println("SETTER IS NOT OF ALLOWED STANDARD FORM");
                           badQuery=true;
                           return;
                       }
                       //push the properties in the stack for further use
                       attributeTypeStack.push(attributeType(condition.toString()));
                       conceptNameStack.push(conceptName(condition.toString()));
                       attributeValueStack.push(attributeValue(condition.toString()));
                      
                       setCondition(condition.toString());
                       i=i-1;
                       continue;
                   }
                   if(booleanString.charAt(i)==' ') {
                       continue;
                   }
                   if(isOperator(booleanString.charAt(i))) {
                       operatorNumber++;
                       if(booleanString.charAt(i)=='&') {
                               if(samePropertyWithAnd==true && booleanString.substring(i+1).indexOf('&')<0 && booleanString.substring(i+1).indexOf('|')<0) {
                                   System.err.println("A BAD QUERY OBJECT/INAPPROPRIATE SETTER OBJECT");
                                   //clear all the values that has been stored so far since the setter object is
                                   //not correct one
                                   unitHashMap.clear();
                                   accuracyHashMap.clear();
                                   rangeHashMap.clear();
                                   badQuery=true;
                                   return;
                               }
                               firstCondition=attributeTypeStack.get(attributeTypeStack.size()-1);
                               secondCondition=attributeTypeStack.get(attributeTypeStack.size()-2);
                               firstConcept=conceptNameStack.get(conceptNameStack.size()-1);
                               secondConcept=conceptNameStack.get(conceptNameStack.size()-2);
                               firstAttributeValue=attributeValueStack.get(attributeValueStack.size()-1);
                               secondAttributeValue=attributeValueStack.get(attributeValueStack.size()-2);

                               //if & is used between two Unit objects
                               if(firstCondition.equals(secondCondition) && firstCondition.equals("unit") && firstConcept.equals(secondConcept) && !firstAttributeValue.equals(secondAttributeValue)) {

                                       if(booleanString.substring(i+1).indexOf('&')<0 && booleanString.substring(i+1).indexOf('|')<0 ) {
                                          // now we are very sure that this Query setter condition cant hold true since a & between units
                                          // & no other operator is present after that
                                          System.err.println("A BAD QUERY OBJECT/INAPPROPRIATE SETTER OBJECT");
                                          badQuery=true;
                                          return;
                                       }
                                       //wait for the next & to decide the Query failure
                                       samePropertyWithAnd=true;
                                       unitList=unitHashMap.get(firstConcept);
                                       //remove the already stored unit conditions from unitHashMap
                                       for(int k=0;k<2;k++) {

                                           unitList.remove(unitList.size()-1);
                                       }
                                       unitHashMap.put(firstConcept,unitList);
                               }
                               //if & is used between Accuracy Objects
                               if(firstCondition.equals(secondCondition) && firstCondition.equals("accuracy") && firstConcept.equals(secondConcept) ) {

                                       accuracyList=accuracyHashMap.get(firstConcept);
                                       try {
                                           netAccuracy=IntervalOperations.intersection(accuracyList.get(accuracyList.size()-1),accuracyList.get(accuracyList.size()-2));
                                            //if the intersection of both Accuracy is empty
                                           if(netAccuracy==null) {
                                              //if no further operator is present then we are sure that setter
                                              //condition can't hold true
                                              if(booleanString.substring(i+1).indexOf('&')<0 && booleanString.substring(i+1).indexOf('|')<0) {
                                                  System.err.println("A BAD QUERY OBJECT/INAPPROPRIATE SETTER OBJECT");
                                                  badQuery=true;
                                                  return;
                                              }
                                              // wait for the next operator
                                              else
                                                  samePropertyWithAnd=true;
                                           }
                                           //remove the already stroed Accuracy conditions from accuracyHashMap
                                           for(int k=0;k<2;k++) {
                                               accuracyList.remove(accuracyList.size()-1);
                                           }
                                           //since here we are sure intersection is not empty let's store the intersection
                                           //in the HashMap
                                           accuracyList.add(netAccuracy);
                                           accuracyHashMap.put(firstConcept,accuracyList);
                                     }
                                       catch(ArrayIndexOutOfBoundsException ex) {
                                           continue;
                                       }
                             }
                              //if & is used between Range Objects
                              if(firstCondition.equals(secondCondition) && firstCondition.equals("range") && firstConcept.equals(secondConcept) ) {

                                       rangeList=rangeHashMap.get(firstConcept);
                                       try {
                                           netRange=IntervalOperations.intersection(rangeList.get(rangeList.size()-1),rangeList.get(rangeList.size()-2));
                                           //if the intersection of both Accuracy is empty
                                           if(netRange==null) {
                                             //if no further operator is present then we are sure that setter
                                             //condition can't hold true
                                              if(booleanString.substring(i+1).indexOf('&')<0 && booleanString.substring(i+1).indexOf('|')<0) {
                                                  System.err.println("A BAD QUERY OBJECT/INAPPROPRIATE SETTER OBJECT");
                                                  badQuery=true;
                                                  return;
                                              }
                                               // wait for the next operator
                                              else
                                                  samePropertyWithAnd=true;
                                           }
                                           //remove the already stroed Accuracy conditions from accuracyHashMap
                                           for(int k=0;k<2;k++) {

                                               rangeList.remove(rangeList.size()-1);
                                           }
                                           //since here we are sure intersection is not empty let's store the intersection
                                           //in the HashMap
                                           rangeList.add(netRange);
                                           rangeHashMap.put(firstConcept,rangeList);
                                     }
                                       catch(ArrayIndexOutOfBoundsException ex) {
                                           continue;
                                       }
                             }
                       }
                       else {//since it is | operator which can't a make a query fail so we delete 1 last existing conditions
                           attributeTypeStack.pop();
                           conceptNameStack.pop();
                           attributeValueStack.pop();
                       }
                   }

               }//this is to show the unit constraint present in the hashmap
                //to be checked in console at the time of test
               //printUnitHashMap();
    }

    /**
      * it is used to view RangehashMap in a a user friendly manner which is easy to visualize
    */
    public void printAccuracyHashMap() {

                for(Iterator it=accuracyHashMap.keySet().iterator();it.hasNext();) {
                    String key=(String) it.next();
                    System.out.println(accuracyHashMap.get(key).size());
                    for(int j=0;j<accuracyHashMap.get(key).size();j++) {
                        accuracyHashMap.get(key).get(j).print();

                    }

                }
    }

     /**
      * it is used to view RangehashMap in a a user friendly manner which is easy to visualize
      */
    public void printRangeHashMap() {

                for(Iterator it=rangeHashMap.keySet().iterator();it.hasNext();) {
                    String key=(String) it.next();
                    for(int j=0;j<rangeHashMap.get(key).size();j++) {
                        rangeHashMap.get(key).get(j).print();

                    }

                }
    }
    /**
         * it gets call from constructor for converting the given boolean expression to it postfix form
	 * @param infixExpr: infix form of boolean expression
         * @return string of postfix form
	 */

   private String convert2Postfix(String infixExpr) {
                char[] chars = infixExpr.toCharArray();
                Stack<Character> stack=new Stack();
                StringBuilder out = new StringBuilder(infixExpr.length());

                for (char c : chars) {
                        if (isOperator(c)) {
                                out.append(" ");
                                if(stack.isEmpty())
                                {
                                  //stack.push(c);
                                }
                                while (!stack.isEmpty() &&  stack.peek() != '(') {
                                        if (operatorGreaterOrEqual(stack.peek(), c)) {
                                           // out.append(" ");
                                            out.append(stack.pop());

                                        } else {

                                                break;
                                        }
                                }
                                stack.push(c);
                        } else if (c == '(') {
                                stack.push(c);
                        } else if (c == ')') {

                                while (!stack.isEmpty() && stack.peek() != (Object)'(') {

                                        out.append(stack.pop());

                                }
                                if (!stack.isEmpty()) {
                                        out.append(" ");
                                        stack.pop();
                                }
                        } else if (isAlphaNum(c)) {

                                out.append(c);
                        }
                }
                while (!stack.empty()) {
                        //System.out.println("jhjf");
                        out.append(stack.pop());

                }
                return out.toString();
        }
  

        
    

              /**
         * used to parse a Where syntax(conceptName1.unit.equals(""))
	 * return position of nth occurrence of a character in a string
	 * @param formula: a String
	 * @param c: the character need to be searched
	 * @param pos: pos occurrence of the character
	 * @return the position of nth occurrence
	 */
            private int n_indexof(String formula,char c,int pos) {

                            int position;
                            position= formula.indexOf(c,0);
                            while (pos--> 1 && position != -1) {
                                position = formula.indexOf(c, position+1);
                                //System.out.println(position);

                            }
                            return position;


           }

             /**
         * it gets call from the method operatorGreaterOrEqual() for getting priority of operators
	 * @param operator:the character priority of which needs to be determined
         * @return priority(priority of & is greater than |)
	 */

        private int getPrecedence(char operator) {
                int     ret=0;
                if(operator=='&')
                {
                        ret=1;
                }
                return ret;
        }

           /**
         * it gets call from the method convert2Postfix() for comparing two operators
	 * @param op1:first operator
         * @param op2:second operator
         * @return true if op1 is of high priority than op2
	 */

        private boolean operatorGreaterOrEqual(char op1, char op2) {
                return getPrecedence(op1)>= getPrecedence(op2);
        }

           /**
         * it gets call from the method convert2Postfix() for checking if
         * a character is a operator(&,|)
	 * @param val:character which needs to be checked
         * @return true if val is a operator
	 */
        private boolean isOperator(char val) {
                return operators.indexOf(val)>= 0;
        }
            /**
         * it gets call from the method convert2Postfix() for checking if
         * a character is of alphanumeric type or not
	 * @param val:character which needs to be checked
         * @return true if val is of alphanumeric type or not
	 */

        private boolean isAlphaNum(char val) {
                return Character.isLetterOrDigit(val)||specialChars.indexOf(val)>=0;
        }
        //for accessing the Unit HashMap
        public HashMap<String,ArrayList<Unit>> getUnitHashMap() {
                return unitHashMap;
        }
        //for accessing the Unit HashMap
        public void setUnitHashMap(String conceptName,ArrayList<Unit> unitList) {
                unitHashMap.put(conceptName,unitList);
        }

        //for accessing the Accuracy HashMap
        public HashMap<String,ArrayList<Accuracy>> getAccuracyHashMap() {
                return accuracyHashMap;
        }
        //for accessing the Accuracy HashMap
        public void setAccuracyHashMap(String conceptName,ArrayList<Accuracy> accuracyList) {
                accuracyHashMap.put(conceptName,accuracyList);
        }

        //for accessing the Range HashMap
        public HashMap<String,ArrayList<Range>> getRangeHashMap() {
                return rangeHashMap;
        }
        //for accessing the Range HashMap
        public void setRangeHashMap(String conceptName,ArrayList<Range> rangeList) {
                rangeHashMap.put(conceptName,rangeList);
        }
        public boolean getBadQuery() {
            return badQuery;
        }
    
}
