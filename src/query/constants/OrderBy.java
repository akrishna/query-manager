/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package query.constants;

import sparqlfunctions.constants.Concept;
import sparqlfunctions.constants.Parameter;
import sparqlfunctions.constants.Formula;


/**
 *
 * @author krishna
 */
public class OrderBy {
    
    private String orderByComponentName;
    public<T> OrderBy(T orderByValue) {
        
            String componentName=orderByValue.getClass().toString();
            //  System.out.println(componentName);
            int j=componentName.lastIndexOf('.');
            // System.out.println(componentName.substring(j+1)+" "+selectorList[i]+" "+((Concept)selectorList[i]).getConceptName());
            if(componentName.substring(j+1).equals("Formula")) {
                orderByComponentName=((Formula)orderByValue).getformulaName();
            }
            else if(componentName.substring(j+1).equals("Parameter")) {
                orderByComponentName=((Parameter)orderByValue).getParameterName();
            }
            else if(componentName.substring(j+1).equals("Concept")) {
                orderByComponentName=((Concept)orderByValue).getConceptName();
            }
            else {
                orderByComponentName=orderByValue.toString();
            }
     }
    
   
  
    public void printComponentName() {
       
           System.out.println(orderByComponentName);
        
    }
    
    
    
}
